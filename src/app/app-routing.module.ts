import { ExtraOptions, RouterModule, Routes, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import { LoginComponent } from './pages/login/login.component';
import { RastreamentoClienteComponent } from './pages/rastreamento-cliente/rastreamento-cliente.component';
import { AuthGuard } from './guards/auth-guard';


const routes: Routes = [

  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule', canActivate:[AuthGuard]},

  {
    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,

      },
      {
        path: 'login',
        component: LoginComponent,

      },
      {
        path: 'register',
        component: NbRegisterComponent,

      },
      {
        path: 'logout',
        component: NbLogoutComponent,

      },
      {
        path: 'request-password',
        component: NbRequestPasswordComponent,
      },

    ],
  },
  { path: 'cliente-rastreamento', component: RastreamentoClienteComponent },
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },

  { path: '**', redirectTo: 'auth/login' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}


