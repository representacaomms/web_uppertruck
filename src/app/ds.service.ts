import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";



@Injectable()

export class DsService implements CanActivate  {

    constructor() { }
    private isAuthenticated: boolean = false;
  
    canActivate() {
      return this.isAuthenticated;
    }

  
}
