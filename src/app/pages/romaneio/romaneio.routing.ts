import { Routes, RouterModule }  from '@angular/router';

import { Romaneio } from './romaneio.component';
import { ListarColetasComponent } from './movColetas/listar-coletas';
import { EditarColetas } from './movColetas/editar_coletas';


import { Coletas } from './movColetas/coletas';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Romaneio,
    children: [
      { path: 'coletas', component: Coletas},
      { path: 'listar-romaneios', component: ListarColetasComponent},
      { path: 'editar-romaneio/:id', component: EditarColetas},
      

    ]
  }
];

export const routing = RouterModule.forChild(routes);
