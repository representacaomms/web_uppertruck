import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './romaneio.routing';

import { Romaneio } from './romaneio.component';
import { Coletas } from './movColetas/coletas';
import { ListarColetasComponent } from './movColetas/listar-coletas';
import { EditarColetas } from './movColetas/editar_coletas';
import { ThemeModule } from '../../@theme/theme.module';
import { ToastModule } from 'primeng/toast';

import { ModalComponent } from './modals/modal/modal.component';
import { ModalsComponent } from './modals/modals.component';
import { ModalComponentCliente } from './modals/modal/modal.component.cliente';

const components = [
  ModalsComponent,
  ModalComponent,

];

@NgModule({
  entryComponents: [
    ModalComponent,
    ModalComponentCliente,
    ListarColetasComponent,
  ],
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    ThemeModule,
    ToastModule
  ],
  declarations: [
    Coletas,
    Romaneio,
    ListarColetasComponent,
    EditarColetas,    
    ModalsComponent,
    ModalComponent,
    ModalComponentCliente
  ]
})
export class RomaneioModule {
}
