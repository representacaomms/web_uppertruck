import { Component, ViewContainerRef, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../../data.service';
import { DsService } from '../../../../ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ModalsComponent } from '../../modals/modals.component';


@Component({
    selector: 'editar_coletas',
    templateUrl: './editar_coletas.html',
	providers:[ModalsComponent]

})
export class EditarColetas {
    data: any = {};
    coleta: any = {};
    entrega: any = {};
    id: any;
    dadosRomaneios: any = {};
    dColetas: any = {};
    dVeiculos: any = {};
	dClientes: any = {};
    romaneios: any = [];
    motoristas: any = [];
    cnpj: any;
    relMotoristas: any = {};
	relClientes: any = {};
    
    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService,
        private router: Router, private modal: ModalsComponent) {

        this.route.params.subscribe(res => {
            this.id = res.id;
        });

    }

    ngOnInit() {        
        this.data.data_admi = this.service.pegarData();

		this.getClientes();
        this.getMotoristas();
        this.getListar();
    }
    getListar() {
        let tabela = 'romaneios'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })
    }

    populardadosCNPJ(dados) {
        console.log(dados)
        if(dados.data === undefined){
            this.coleta.cnpj_cliente = dados['coleta']['cnpj_cliente'];
            this.coleta.cubagem = dados['coleta']['cubagem'];
            this.coleta.endereco_cliente = dados['coleta']['endereco_cliente'];
            this.coleta.frete_motorista = dados['coleta']['frete_motorista'];
            this.coleta.frete_negociado = dados['coleta']['frete_negociado'];
            this.coleta.peso = dados['coleta']['peso'];
            this.coleta.posAtual = dados['coleta']['posAtual'];
            this.coleta.razao_social_cliente = dados['coleta']['razao_social_cliente'];
            this.coleta.telefone_cliente = dados['coleta']['telefone_cliente'];
            this.coleta.tipo_veiculo = dados['coleta']['tipo_veiculo'];
            this.coleta.volume = dados['coleta']['volume'];
            this.coleta.status = dados['coleta']['status'];
    
    
            this.entrega.cnpj_cliente = dados['entrega']['cnpj_cliente'];
            this.entrega.cubagem = dados['entrega']['cubagem'];
            this.entrega.endereco_cliente = dados['entrega']['endereco_cliente'];
            this.entrega.frete_motorista = dados['entrega']['frete_motorista'];
            this.entrega.frete_negociado = dados['entrega']['frete_negociado'];
            this.entrega.peso = dados['entrega']['peso'];
            this.entrega.posAtual = dados['entrega']['posAtual'];
            this.entrega.razao_social_cliente = dados['entrega']['razao_social_cliente'];
            this.entrega.telefone_cliente = dados['entrega']['telefone_cliente'];
            this.entrega.tipo_veiculo = dados['entrega']['tipo_veiculo'];
            this.entrega.volume = dados['entrega']['volume'];
        } else {

        this.data.celular = dados['data']['celular'];
        this.data.cpf = dados['data']['cpf'];
        this.data.data_admi = dados['data']['data_admi'];
        this.data.dtNasc = dados['data.dtNasc'];
        this.data.nome = dados['data']['nome'];
        this.data.observacao = dados['data']['observacao'];
        this.data.placa = dados['data']['placa'];
        this.data.rg = dados['data']['rg'];
        this.data.status = dados['data']['status'];

        this.coleta.cnpj_cliente = dados['coleta']['cnpj_cliente'];
        this.coleta.uf_cliente = dados['coleta']['uf_cliente'];
        this.coleta.cidade_cliente = dados['coleta']['cidade_cliente'];
        this.coleta.cubagem = dados['coleta']['cubagem'];
        this.coleta.endereco_cliente = dados['coleta']['endereco_cliente'];
        this.coleta.frete_motorista = dados['coleta']['frete_motorista'];
        this.coleta.frete_negociado = dados['coleta']['frete_negociado'];
        this.coleta.peso = dados['coleta']['peso'];
        this.coleta.posAtual = dados['coleta']['posAtual'];
        this.coleta.razao_social_cliente = dados['coleta']['razao_social_cliente'];
        this.coleta.telefone_cliente = dados['coleta']['telefone_cliente'];
        this.coleta.tipo_veiculo = dados['coleta']['tipo_veiculo'];
        this.coleta.volume = dados['coleta']['volume'];
        this.coleta.status = dados['coleta']['status'];


        this.entrega.cnpj_cliente = dados['entrega']['cnpj_cliente'];
        this.entrega.cidade_cliente = dados['entrega']['cidade_cliente'];
        this.entrega.uf_cliente = dados['entrega']['uf_cliente'];
        this.entrega.cubagem = dados['entrega']['cubagem'];
        this.entrega.endereco_cliente = dados['entrega']['endereco_cliente'];
        this.entrega.frete_motorista = dados['entrega']['frete_motorista'];
        this.entrega.frete_negociado = dados['entrega']['frete_negociado'];
        this.entrega.peso = dados['entrega']['peso'];
        this.entrega.posAtual = dados['entrega']['posAtual'];
        this.entrega.razao_social_cliente = dados['entrega']['razao_social_cliente'];
        this.entrega.telefone_cliente = dados['entrega']['telefone_cliente'];
        this.entrega.tipo_veiculo = dados['entrega']['tipo_veiculo'];
        this.entrega.volume = dados['entrega']['volume'];
        }

    }

    atualizar(data, coleta, entrega) { 
        let value = {data: data, coleta: coleta, entrega: entrega}
        let tabela ='romaneios';
        this.service.updateDoc(tabela, this.id, value).subscribe(result => {
            this.service.makeToast('success', 'Romaneio editando com sucesso!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/romaneio/listar-romaneios`])
            }, 3500);
        })       

        // this.service.makeToast('success', 'Romaneio editado com sucesso!', 'Você será redirecionado!');
        setTimeout(() => {
            this.router.navigate([`pages/romaneio/listar-romaneios`])
        }, 3500); 

    }

    

    cancelar() {
        //  this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/romaneio/listar-romaneios`])
        }, 3500);
    }

    getMotoristas() {
        let tabela =   'motoristas';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.relMotoristas = resultado			
		}) 
	}   

	getClientes() {
        let tabela =   'clientes';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.relClientes = resultado			
		}) 
	} 

	
    buscarDadosMotorista(e) {
		for (const key in this.relMotoristas) {
		   if (this.relMotoristas[key].cpf == e) {
			   const motorista = this.relMotoristas[key]
			   this.populardadosMotorista(motorista);
		   } 
	   } 

   }

	populardadosMotorista(dados) {
		let data = dados
		console.log(data);
		this.dVeiculos = data;
		this.data.nome = this.dVeiculos['nome'];
		this.data.cpf = this.dVeiculos['cpf'];
		this.data.rg = this.dVeiculos['rg'];
		this.data.telefone_motorista = this.dVeiculos['fone1'];
		this.data.celular = this.dVeiculos['celular'];
		this.data.dtaNasc = this.dVeiculos['dtNasc'];
		this.data.data_admi = this.dVeiculos['data_admi'];
		this.data.status = 'Ativo';
	}

	cadastrarMotorista() {
		this.modal.motorista();
	}

	cadastrarCliente() {
		this.modal.cliente();

	}

	buscarDadosCNPJColeta(e) {
		for (const key in this.relClientes) {
			if (this.relClientes[key].cnpj == e) {
				const cliente = this.relClientes[key]
				this.populardadosClientesColeta(cliente);
			} 
		} 
	}
	populardadosClientesColeta(dados) {
		console.log(dados)
		this.dClientes = dados;
		this.coleta.razao_social_cliente = this.dClientes['razao_social'];
		this.coleta.cnpj_cliente = this.dClientes['cnpj'];
		this.coleta.endereco_cliente = this.dClientes['endereco'];
		this.coleta.telefone_cliente = this.dClientes['telefone'];
		this.coleta.cidade_cliente = this.dClientes['cidade'];
		this.coleta.uf_cliente = this.dClientes['estado'];
	}

	buscarDadosCNPJEntrega(e) {
		for (const key in this.relClientes) {
			if (this.relClientes[key].cnpj == e) {
				const cliente = this.relClientes[key]
				this.populardadosClientesEntrega(cliente);
			} 
		} 		
	}
	populardadosClientesEntrega(dados) {
		this.dClientes = dados;
		this.entrega.razao_social_cliente = this.dClientes['razao_social'];
		this.entrega.cnpj_cliente = this.dClientes['cnpj'];
		this.entrega.endereco_cliente = this.dClientes['endereco'];
		this.entrega.telefone_cliente = this.dClientes['telefone'];
		this.entrega.cidade_cliente = this.dClientes['cidade'];
		this.entrega.uf_cliente = this.dClientes['estado'];
	}





}
