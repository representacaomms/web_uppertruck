import { Component, ViewContainerRef, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DsService } from '../../../../ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { DataService } from '../../../../data.service';
import { ModalsComponent } from '../../modals/modals.component';

@Component({
	selector: 'coletas',
	templateUrl: './coletas.html',
	providers:[ModalsComponent]
})
export class Coletas implements OnInit {
	data: any = {};
	entrega: any = {};
	coleta: any = {};
	key = "AIzaSyAdMlm7O6wQmM6edaFooaYbNpaiNGQrI94";
	coordenadas: any = {};
	coordenadasEnt: any = {};
	dVeiculos: any = {};
	dClientes: any = {};
	motoristas: any = [];
	clientes: any = [];
	romaneios: any = [];
	cnpj: any;
	relMotoristas: any = {};
  relClientes: any = {};
  placa: any;

	constructor(private http: Http, public router: Router, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService, public service: DataService,
	private modal: ModalsComponent) {
	}
	ngOnInit() {
		this.data.data_admi = this.service.pegarData();

		this.getClientes();
		this.getMotoristas();
	}

	pegarCoordenadas(endereco) {
		this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${endereco}&key=${this.key}`).subscribe(dados => {
			this.coordenadas = JSON.parse(dados["_body"]);
			console.log(this.coordenadas)
			if (this.coordenadas['status'] != "OK") {
				let mensagem = 'Dica: Nome da rua, cidade estado';
				this.showError(mensagem);
			} else {
				setTimeout(() => {
					this.data.latitude = this.coordenadas['results'][0]['geometry']['location']['lat'];
					this.data.longitude = this.coordenadas['results'][0]['geometry']['location']['lng'];
				}, 100);
			}
		})

	}

	pegarCoordenadasEntrega(endereco) {
		console.log(endereco)
		this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${endereco}&key=${this.key}`).subscribe(dados => {
			this.coordenadasEnt = JSON.parse(dados["_body"]);
			console.log(this.coordenadas)
			if (this.coordenadasEnt['status'] != "OK") {
				let mensagem = 'Dica: Nome da rua, cidade estado';
				this.showError(mensagem);
			} else {
				setTimeout(() => {
					this.data.latitudeEntrega = this.coordenadasEnt['results'][0]['geometry']['location']['lat'];
					this.data.longitudeEntrega = this.coordenadasEnt['results'][0]['geometry']['location']['lng'];
				}, 100);
			}
		})
	}

	showError(mensagem) {
		//this.toastr.error(mensagem, 'Oops, Endereço incorreto !');
	}

	getMotoristas() {
        let tabela =   'motoristas';
		this.service.getAllDoc(tabela).subscribe( result =>{

		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;
			resultado.push(result[key])
		}
		this.relMotoristas = resultado
		})
	}

	getClientes() {
        let tabela =   'clientes';
		this.service.getAllDoc(tabela).subscribe( result =>{

		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;
			resultado.push(result[key])
		}
		this.relClientes = resultado
		})
  }




    buscarDadosMotorista(e) {
		for (const key in this.relMotoristas) {
		   if (this.relMotoristas[key].cpf == e) {
			   const motorista = this.relMotoristas[key]
			   this.populardadosMotorista(motorista);
		   }
	   }

   }

	populardadosMotorista(dados) {
    this.getVeiculos(dados.cpf);
    setTimeout(() => {
      let data = dados
      console.log(data);
      this.dVeiculos = data;
      this.data.nome = this.dVeiculos['nome'];
      this.data.cpf = this.dVeiculos['cpf'];
      this.data.rg = this.dVeiculos['rg'];
      this.data.telefone_motorista = this.dVeiculos['fone1'];
      this.data.celular = this.dVeiculos['celular'];
      this.data.dtaNasc = this.dVeiculos['dtNasc'];
      this.data.data_admi = this.dVeiculos['data_admi'];
      this.data.status = 'Ativo';
      this.data.placa = this.placa;

    }, 1000);

  }

  getVeiculos(cpf) {
    let tabela = 'veiculos';
    this.service.getAllDoc(tabela).subscribe(result => {

      for (const key in result) {
        if (result[key].cnpj === cpf) {
          const placa = result[key].placa;
          this.placa =  placa;
          }
      }
    });

  }


	cadastrarMotorista() {
		this.modal.motorista();
	}

	cadastrarCliente() {
		this.modal.cliente();

	}

	buscarDadosCNPJColeta(e) {
		for (const key in this.relClientes) {
			if (this.relClientes[key].cnpj == e) {
				const cliente = this.relClientes[key]
				this.populardadosClientesColeta(cliente);
			}
		}
	}
	populardadosClientesColeta(dados) {
		console.log(dados)
		this.dClientes = dados;
		this.coleta.razao_social_cliente = this.dClientes['razao_social'];
		this.coleta.cnpj_cliente = this.dClientes['cnpj'];
		this.coleta.endereco_cliente = this.dClientes['endereco'];
		this.coleta.telefone_cliente = this.dClientes['telefone'];
		this.coleta.cidade_cliente = this.dClientes['cidade'];
		this.coleta.uf_cliente = this.dClientes['estado'];
	}

	buscarDadosCNPJEntrega(e) {
		for (const key in this.relClientes) {
			if (this.relClientes[key].cnpj == e) {
				const cliente = this.relClientes[key]
				this.populardadosClientesEntrega(cliente);
			}
		}
	}
	populardadosClientesEntrega(dados) {
		this.dClientes = dados;
		this.entrega.razao_social_cliente = this.dClientes['razao_social'];
		this.entrega.cnpj_cliente = this.dClientes['cnpj'];
		this.entrega.endereco_cliente = this.dClientes['endereco'];
		this.entrega.telefone_cliente = this.dClientes['telefone'];
		this.entrega.cidade_cliente = this.dClientes['cidade'];
		this.entrega.uf_cliente = this.dClientes['estado'];
	}
	cadastrar(data,coleta,entrega){
		let value = {data: data, coleta: coleta, entrega: entrega}
        let tabela ='romaneios';
        this.service.createDoc(value, tabela).subscribe(
          res => {
            this.service.makeToast('success', 'Concluido!', 'Cadastrado com sucesso!');
            setTimeout(() => {
                this.router.navigate([`pages/romaneio/listar-romaneios`]);
            }, 3000);

        })
	}
	cancelar(){
		this.service.makeToast('success', 'Alerta!', 'Você sera redirecionado...!');
            setTimeout(() => {
                this.router.navigate([`pages/romaneio/listar-romaneios`]);
            }, 3000);
	}
	/* 16635025000714
	07045463000119
	16764156883 */

}
