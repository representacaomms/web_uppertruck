import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../../data.service';
import { DsService } from '../../../../ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
    selector: 'app-listar-coletas',
    templateUrl: './listar-coletas.component.html',
    styleUrls: ['./listar-coletas.component.scss']
})
export class ListarColetasComponent implements OnInit {

    romaneios: any = [];
    getColetas: any = [];
    cnpj: any;

    constructor(private http: Http, private router: Router,  public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService) {
    }

    ngOnInit() {
		this.getListar();	

    }

    getListar() {
        let tabela =   'romaneios';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
        this.getColetas = resultado	
        console.log(resultado)		
		}) 
    }

    cadastrar() {
        this.router.navigate([`pages/romaneio/coletas`])
    }

    cancelar() {
        this.router.navigate([`pages/romaneio/listar-coletas`])
    }

    visualizarDados(message) {
        //this.router.navigate([`pages/crud/visualizarColetas/${message._id}`])
    }
    editarDados(message) {
        this.router.navigate([`pages/romaneio/editar-romaneio/${message.id}`])

    }
    

    getItems(ev) {
        // Reset items back to all of the items
        this.getColetas;

        // set val to the value of the ev target
        var val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.getColetas = this.getColetas.filter((item) => {
                console.log(item)
                return (
                    item['coleta']['razao_social_cliente'].toLowerCase().indexOf(val) > -1 || 
                    item['entrega']['razao_social_cliente'].toLowerCase().indexOf(val) > -1
                    );
            })
        } else {
            this.ngOnInit();
        }
    }

}
