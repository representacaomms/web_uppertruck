import {Component} from '@angular/core';

@Component({
  selector: 'romaneio',
  template: `<router-outlet></router-outlet>`
})
export class Romaneio {

  constructor() {
  }
}
