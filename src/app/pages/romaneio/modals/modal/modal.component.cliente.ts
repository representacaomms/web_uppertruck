import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-modal',
  templateUrl: './modal.component.cliente.html',
  styleUrls: ['./modal.component.scss'],

})
export class ModalComponentCliente implements OnInit{

  modalHeader: string;
  modalContent = ` Esse Motorista é Proprietário?`;
  conteudo = this.conteudo;
  listar: any = [];

  constructor(private activeModal: NgbActiveModal, private service: DataService, private router: Router,) { }
  ngOnInit() {
		
		let tabela =   'clientes';
		this.service.getAllDoc(tabela).subscribe(result => {
			const resultado = [];
			for (const key in result) {
				result[key].id = key;
				resultado.push(result[key])
			}
			this.listar = resultado
			console.log(this.listar)
		})
	}

	adicionar() {
		this.router.navigate([`pages/clientes/adicionar-clientes`])
	}
	
	getItems(ev) {
		// Reset items back to all of the items
		this.listar;

		// set val to the value of the ev target
		var val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.listar = this.listar.filter((item) => {
				return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		} else {
			this.ngOnInit();
		}
	}
  
}
