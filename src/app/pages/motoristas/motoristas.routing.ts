import { Routes, RouterModule }  from '@angular/router';


import { Motoristas } from './motoristas';
import { ListarMotoristasComponent } from './listar-motoristas/listar-motoristas.component';
import { VisualizarMotoristas } from './visualizar_motoristas';
import { EditarMotoristas } from './editar_motoristas';
import { AdicionarMotoristas } from './adicionar-motoristas';
import { FotosMotorista } from './fotosMotorista';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Motoristas,
    children: [
       { path: 'listar-motoristas', component: ListarMotoristasComponent},
       { path: 'adicionar-motoristas', component: AdicionarMotoristas},
       { path: 'visualizar-motoristas/:id', component: VisualizarMotoristas},
       { path: 'editar-motoristas/:id', component: EditarMotoristas},
       { path: 'mobile-motoristas/:id', component: AdicionarMotoristas},
       { path: 'fotos-motoristas/:id', component: FotosMotorista},


    ]
  }
];

export const routing = RouterModule.forChild(routes);
