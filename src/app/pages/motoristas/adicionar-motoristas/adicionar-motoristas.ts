import { Component, ViewContainerRef, OnInit, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationService, MessageService } from 'primeng/api';
import {SelectItem} from 'primeng/api';


@Component({
    selector: 'adicionar-motoristas',
    templateUrl: './adicionar-motoristas.html',
    styleUrls: ['./adicionar-motoristas.scss']

})
export class AdicionarMotoristas implements OnInit {
    dadosMotoristas: any = {};
    dMotoristas: any = {};
    motoristas: any;
    proprietarios:any;
    cnpj: any;
    msgs: any[] = [];


    situacao: SelectItem[];
    est_civil: SelectItem[];
    uf_rg: SelectItem[];
    cnh_categoria: SelectItem[];

    constructor(private http: Http, private router: Router, private service: DataService, public ds: DsService, private messageService: MessageService,
        @Inject(SESSION_STORAGE) private storage: StorageService, private modalService: NgbModal, private confirmationService: ConfirmationService, private route: ActivatedRoute) {
            this.route.params.subscribe(res => {
                this.dadosMotoristas.cpf = res.cnpj;
                this.dadosMotoristas.device = res.device;
            });
    }


    ngOnInit() {
        this.dadosMotoristas.data_admi = this.service.pegarData();
        this.motoristas = "this.ds.ds.login()";
        this.proprietarios = "this.ds.ds.login()";

        this.situacao = [
            {label:'ATIVO', value:'ATIVO'},
            {label:'INATIVO', value:'INATIVO'},
            {label:'CANCELADO', value:'CANCELADO'},
            {label:'BLOQUEADO', value:'BLOQUEADO'},
        ];
        this.est_civil = [
            {label:'CASADO', value:'CASADO' },
            {label:'SOLTEIRO', value:'SOLTEIRO' },
            {label:'AMASIADO', value:'AMASIADO' },
            {label:'DIVORCIADO', value:'DIVORCIADO' },
            {label:'VÍUVO', value:'VÍUVO' },
            {label:'UNIAO ESTÁVEL', value:'UNIAO ESTÁVEL' },
        ];

        this.uf_rg = [
            {value:'ac', label:'Acre'}, 
            {value:'al', label:'Alagoas'}, 
            {value:'am', label:'Amazonas'}, 
            {value:'ap', label:'Amapá'}, 
            {value:'ba', label:'Bahia'}, 
            {value:'ce', label:'Ceará'}, 
            {value:'df', label:'Distrito Federal'}, 
            {value:'es', label:'Espírito Santo'}, 
            {value:'go', label:'Goiás'}, 
            {value:'ma', label:'Maranhão'}, 
            {value:'mt', label:'Mato Grosso'}, 
            {value:'ms', label:'Mato Grosso do Sul'}, 
            {value:'mg', label:'Minas Gerais'}, 
            {value:'pa', label:'Pará'}, 
            {value:'pb', label:'Paraíba'}, 
            {value:'pr', label:'Paraná'}, 
            {value:'pe', label:'Pernambuco'}, 
            {value:'pi', label:'Piauí'}, 
            {value:'rj', label:'Rio de Janeiro'}, 
            {value:'rn', label:'Rio Grande do Norte'}, 
            {value:'ro', label:'Rondônia'}, 
            {value:'rs', label:'Rio Grande do Sul'}, 
            {value:'rr', label:'Roraima'}, 
            {value:'sc', label:'Santa Catarina'}, 
            {value:'se', label:'Sergipe'}, 
            {value:'sp', label:'São Paulo'}, 
            {value:'to', label:'Tocantins'}, 
        ];

        this.cnh_categoria = [
            {value:'A', label:'A'}, 
            {value:'B', label:'B'}, 
            {value:'C', label:'C'}, 
            {value:'D', label:'D'}, 
            {value:'E', label:'E'}, 
            {value:'AB', label:'AB'}, 
            {value:'AC', label:'AC'}, 
            {value:'AE', label:'AE'}, 
            {value:'BC', label:'BC'}, 
            {value:'CD', label:'CD'}, 
           
            
        ];
    }

    showConfirm(dados) {
        this.messageService.clear();
        this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:'Você tem certeza?', detail:'Confirme para prosseguir', data: dados});
    }

    confirmacao(dados) {
        dados.celular = dados.celular.replace(/\D/g, '');
        const result = dados;
        this.confirmationService.confirm({
            message: 'Deseja cadastrar esse Motorista também como Proprietário?',
            header: 'Confirmação',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.cadastrarMotorista(result);
                this.cadastrarProprietarios(result);
                setTimeout(() => {
                    this.msgs = [{ severity: 'info', summary: 'Confirmado', detail: 'Motorista cadastrado como Proprietário!' }];
                }, 1000); 
                setTimeout(() => {
                    this.router.navigate([`pages/motoristas/listar-motoristas`]);
                }, 2000);   
           
            },
            reject: () => {
                this.cadastrarMotorista(result);
                setTimeout(() => {
                    this.msgs = [{ severity: 'info', summary: 'Rejeitado', detail: 'Motorista cadastrado com sucesso!' }];
                }, 1000);    
                setTimeout(() => {
                    this.router.navigate([`pages/motoristas/listar-motoristas`]);
                }, 2000);    
            }
        });
    }

    buscarDados(e) {
        console.log(e)
        //Nova variável "cep" somente com dígitos.
        var cep = e.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {
                //Sincroniza com o callback.
                this.http.get('https://viacep.com.br/ws/' + cep + '/json')
                    .map(dados => dados.json())
                    .subscribe(dados => this.populacampos(dados))


            } //end if.
            else {
                //cep é inválido.
                this.dadosMotoristas.cep = '';
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            this.dadosMotoristas.cep = '';
        }
    }

    populacampos(dados) {
        this.dMotoristas = dados;
        this.dadosMotoristas.cep = this.dMotoristas['cep'];
        this.dadosMotoristas.complemento = this.dMotoristas['complemento'];
        this.dadosMotoristas.endereco = this.dMotoristas['logradouro'];
        this.dadosMotoristas.numero = this.dMotoristas['numero'];
        this.dadosMotoristas.bairro = this.dMotoristas['bairro'];
        this.dadosMotoristas.cidade = this.dMotoristas['localidade'];
        this.dadosMotoristas.estado = this.dMotoristas['uf'];
    }

    cadastrarMotorista(dados) {
        let tabela ='motoristas';
        this.service.createDoc(dados, tabela)
        .subscribe(
            res => {            
            this.service.makeToast('success', 'Concluido!', 'Cadastrado com sucesso!');
            setTimeout(() => {
                this.router.navigate([`pages/motoristas/listar-motoristas`]); 
            }, 3000);
            
            }
        )
    }

    cadastrarProprietarios(dados) {

    let tabela ='proprietarios';
        this.service.createDoc(dados, tabela)
        .subscribe(
            res => {            
            this.service.makeToast('success', 'Concluido!', 'Cadastrado com sucesso!');
            setTimeout(() => {
                this.router.navigate([`pages/motoristas/listar-motoristas`]); 
            }, 3000);
            
            }
        )
        
    }

    cancelar() {
        this.service.makeToast('info', 'Concluido!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/motoristas/listar-motoristas`]); 
            }, 3000);
    }
   
}
