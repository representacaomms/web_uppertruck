import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './motoristas.routing';

import { Motoristas } from './motoristas';
import { ListarMotoristasComponent } from './listar-motoristas/listar-motoristas.component';
import { VisualizarMotoristas } from './visualizar_motoristas';
import { EditarMotoristas } from './editar_motoristas';
import { AdicionarMotoristas } from './adicionar-motoristas';
import { FotosMotorista } from './fotosMotorista';

import { ThemeModule } from '../../@theme/theme.module';

import { ModalComponent } from './modals/modal/modal.component';
import { ModalsComponent } from './modals/modals.component';

import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ToastModule} from 'primeng/toast';

import {DropdownModule} from 'primeng/dropdown';
import {InputMaskModule} from 'primeng/inputmask';

const components = [
  ModalsComponent,
  ModalComponent,
];


@NgModule({
  entryComponents: [
    ModalComponent,
  
  ],
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    DropdownModule,
    InputMaskModule
    
  ],
  declarations: [
    Motoristas,
    ListarMotoristasComponent,
    VisualizarMotoristas,
    EditarMotoristas,
    AdicionarMotoristas,
    FotosMotorista,
    ModalsComponent,
    ModalComponent,
  ],
  providers: [ ConfirmationService, MessageService]
})
export class MotoristasModule {
}
