import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';


@Component({
    selector: 'visualizar_motoristas',
    templateUrl: './visualizar_motoristas.html',
    styleUrls: ['./visualizar_motoristas.scss']

})
export class VisualizarMotoristas {
    motoristas: any = [];
    id: any;
    dadosMotoristas: any = {};
    dMotoristas: any = {};
    listar: any = [];

    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService,
        private router: Router) {

        this.route.params.subscribe(res => {
            this.id = res.id;            
        });

    }

    ngOnInit() {    
        this.getListar()   
    }
    getListar() {
        let tabela = 'motoristas'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })

    }
    populardadosCNPJ(record) {
        
        const dados = record;

        console.log(dados)
        this.dadosMotoristas.idMotorista = dados['idMotorista'];           
        this.dadosMotoristas.cpf = dados['cpf'];
        this.dadosMotoristas.data_admi = dados['data_admi'];
        this.dadosMotoristas.nome = dados['nome'];
        this.dadosMotoristas.est_civil = dados['est_civil'];
        this.dadosMotoristas.situacao = dados['situacao'];
        this.dadosMotoristas.nome_pai = dados['nome_pai'];
        this.dadosMotoristas.nome_mae = dados['nome_mae'];
        this.dadosMotoristas.telefone1 = dados['telefone1'];
        this.dadosMotoristas.telefone2 = dados['telefone2'];
        this.dadosMotoristas.celular = dados['celular'];
        this.dadosMotoristas.email = dados['email'];
        this.dadosMotoristas.contato = dados['contato'];
        this.dadosMotoristas.dtNasc = dados['dtNasc'];
        this.dadosMotoristas.cep = dados['cep'];
        this.dadosMotoristas.complemento = dados['complemento'];
        this.dadosMotoristas.endereco = dados['endereco'];
        this.dadosMotoristas.numero = dados['numero'];
        this.dadosMotoristas.bairro = dados['bairro'];
        this.dadosMotoristas.cidade = dados['cidade'];
        this.dadosMotoristas.estado = dados['estado'];
        this.dadosMotoristas.rg = dados['rg'];
        this.dadosMotoristas.uf_rg = dados['uf_rg'];
        this.dadosMotoristas.cidade_nasc = dados['cidade_nasc'];
        this.dadosMotoristas.pais = dados['pais'];
        this.dadosMotoristas.cnh_registro = dados['cnh_registro'];
        this.dadosMotoristas.cnh_espelho = dados['cnh_espelho'];
        this.dadosMotoristas.cnh_categoria = dados['cnh_categoria'];
        this.dadosMotoristas.cnh_pri_habilitacao = dados['cnh_pri_habilitacao'];
        this.dadosMotoristas.cnh_dt_emiss = dados['cnh_dt_emiss'];
        this.dadosMotoristas.cnh_renach = dados['cnh_renach'];
        this.dadosMotoristas.ref_nome1 = dados['ref_nome1'];
        this.dadosMotoristas.ref_contato1 = dados['ref_contato1'];
        this.dadosMotoristas.ref_nome2 = dados['ref_nome2'];
        this.dadosMotoristas.ref_contato2 = dados['ref_contato2'];
    }
    atualizar(dados) {
        console.log(dados)
       /*  const motorista = this.motoristas.record.getRecord(`motoristas/${this.id}`);  // todos.delete()

        motorista.set('idMotorista', dados['idMotorista']);           
         motorista.set('cpf', dados['cpf']);
         motorista.set('data_admi', dados['dados.data_admi']);
         motorista.set('nome', dados['dados.nome']);
         motorista.set('est_civil', dados['dados.est_civil']);
         motorista.set('situacao', dados[' dados.situacao']);
         motorista.set('nome_pai', dados['dados.nome_pai']);
         motorista.set('nome_mae', dados['dados.nome_mae']);
         motorista.set('telefone1', dados['dados.telefone1']);
         motorista.set('telefone2', dados['dados.telefone2']);
         motorista.set('celular', dados['dados.celular']);
         motorista.set('email', dados['dados.email']);
         motorista.set('contato', dados['dados.contato']);
         motorista.set('dtNasc', dados['dados.dtNasc']);
         motorista.set('cep', dados['dados.cep']);
         motorista.set('complemento', dados['dados.complemento']);
         motorista.set('endereco', dados['dados.endereco']);
         motorista.set('numero', dados['dados.numero']);
         motorista.set('bairro', dados['dados.bairro']);
         motorista.set('cidade', dados['dados.cidade']);
         motorista.set('estado', dados['dados.estado']);
         motorista.set('rg', dados['rg']);
         motorista.set('uf_rg', dados['dados.uf_rg']);
         motorista.set('cidade_nasc', dados['dados.cidade_nasc']);
         motorista.set('pais', dados['dados.pais']);
         motorista.set('cnh_registro', dados['dados.cnh_registro']);
         motorista.set('cnh_espelho', dados['dados.cnh_espelho']);
         motorista.set('cnh_categoria', dados['dados.cnh_categoria']);
         motorista.set('cnh_pri_habilitacao', dados['dados.cnh_pri_habilitacao']);
         motorista.set('cnh_dt_emiss', dados['dados.cnh_dt_emiss']);
         motorista.set('cnh_renach', dados['dados.cnh_renach']);
         motorista.set('ref_nome1', dados['dados.ref_nome1']);
         motorista.set('ref_contato1', dados['dados.ref_contato1']);
         motorista.set('ref_nome2', dados['dados.ref_nome2']);
         motorista.set('ref_contato2', dados['dados.ref_contato2']); */

         this.service.makeToast('info','Informação!','Você será redirecionado');
         setTimeout(() => {
            this.router.navigate([`pages/motoristas/editar-motoristas/${this.id}`])
        }, 3500);

    }
    cancelar() {
       // this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/motoristas/listar-motoristas`])
        }, 3500);
    }

}