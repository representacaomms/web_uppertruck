import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
    selector: 'app-listar-motoristas',
    templateUrl: './listar-motoristas.component.html',
    styleUrls: ['./listar-motoristas.component.scss']
})
export class ListarMotoristasComponent implements OnInit {


    motoristas: any = [];
    listar: any = [];
    getMotoristas: any = [];
    imagePath: any;
    cnpj: any;

    constructor(private http: Http, private router: Router,  public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService) {
       
    }

    ngOnInit() {
		this.getListar();   
    }

    adicionar() {
        this.router.navigate([`pages/motoristas/adicionar-motoristas`])
    }
    visualizarDados(message) {
        this.router.navigate([`pages/motoristas/visualizar-motoristas/${message.id}`])
    }
    editarDados(message) {
        this.router.navigate([`pages/motoristas/editar-motoristas/${message.id}`])

    }
    getItems(ev) {
        // Reset items back to all of the items
        this.motoristas;

        // set val to the value of the ev target
        var val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.motoristas = this.motoristas.filter((item) => {
                return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.ngOnInit();
        }
    }


    getListar() {
		let tabela =   'motoristas';
		this.service.getAllDoc(tabela).subscribe( result =>{
            console.log(result)
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.listar = resultado			
		}) 
		
	}
    excluirDados() {

    }

    fotos(cpf) {
        this.router.navigate([`pages/motoristas/fotos-motoristas/${cpf}`])
    }

}
