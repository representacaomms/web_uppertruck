import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';


@Component({
    selector: 'editar_motoristas',
    templateUrl: './editar_motoristas.html',
    styleUrls: ['./editar_motoristas.scss']

})
export class EditarMotoristas {
    motoristas: any = [];
    id: any;
    dadosMotoristas: any = {};
    dMotoristas: any = {};
    listar: any = [];

    constructor(private http: Http, private route: ActivatedRoute,  public service: DataService, public ds: DsService,
        private router: Router) {

        this.route.params.subscribe(res => {
            this.id = res.id;
        });

    }

    ngOnInit() {

        this.getListar()

    }
    getListar() {
        let tabela = 'motoristas'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })

    }
    populardadosCNPJ(record) {

        const dados = record;

        console.log(dados)
        this.dadosMotoristas.idMotorista = dados['idMotorista'];
        this.dadosMotoristas.cpf = dados['cpf'];
        this.dadosMotoristas.data_admi = dados['data_admi'];
        this.dadosMotoristas.nome = dados['nome'];
        this.dadosMotoristas.est_civil = dados['est_civil'];
        this.dadosMotoristas.situacao = dados['situacao'];
        this.dadosMotoristas.senha = dados['senha'];
        this.dadosMotoristas.nome_pai = dados['nome_pai'];
        this.dadosMotoristas.nome_mae = dados['nome_mae'];
        this.dadosMotoristas.telefone1 = dados['telefone1'];
        this.dadosMotoristas.telefone2 = dados['telefone2'];
        this.dadosMotoristas.celular = dados['celular'];
        this.dadosMotoristas.email = dados['email'];
        this.dadosMotoristas.contato = dados['contato'];
        this.dadosMotoristas.data_cadastro = dados['data_cadastro'];
        this.dadosMotoristas.dtNasc = dados['dtNasc'];
        this.dadosMotoristas.cep = dados['cep'];
        this.dadosMotoristas.complemento = dados['complemento'];
        this.dadosMotoristas.endereco = dados['endereco'];
        this.dadosMotoristas.numero = dados['numero'];
        this.dadosMotoristas.bairro = dados['bairro'];
        this.dadosMotoristas.cidade = dados['cidade'];
        this.dadosMotoristas.estado = dados['estado'];
        this.dadosMotoristas.rg = dados['rg'];
        this.dadosMotoristas.uf_rg = dados['uf_rg'];
        this.dadosMotoristas.cidade_nasc = dados['cidade_nasc'];
        this.dadosMotoristas.pais = dados['pais'];
        this.dadosMotoristas.cnh_registro = dados['cnh_registro'];
        this.dadosMotoristas.cnh_espelho = dados['cnh_espelho'];
        this.dadosMotoristas.cnh_categoria = dados['cnh_categoria'];
        this.dadosMotoristas.cnh_pri_habilitacao = dados['cnh_pri_habilitacao'];
        this.dadosMotoristas.cnh_dt_emiss = dados['cnh_dt_emiss'];
        this.dadosMotoristas.cnh_renach = dados['cnh_renach'];
        this.dadosMotoristas.ref_nome1 = dados['ref_nome1'];
        this.dadosMotoristas.ref_contato1 = dados['ref_contato1'];
        this.dadosMotoristas.ref_nome2 = dados['ref_nome2'];
        this.dadosMotoristas.ref_contato2 = dados['ref_contato2'];
    }
    atualizar(dados) {
        console.log(dados)
        let tabela = 'motoristas'
        this.service.updateDoc(tabela, this.id, dados).subscribe(result => {
            this.service.makeToast('success', 'Motorista editando com sucesso!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/motoristas/listar-motoristas`])
            }, 3500);
        })       

    }
    cancelar() {
        this.service.makeToast('info', 'Concluido!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/motoristas/listar-motoristas`]); 
            }, 3000);
    }

    buscarDados(e) {
        console.log(e)
        //Nova variável "cep" somente com dígitos.
        var cep = e.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {
                //Sincroniza com o callback.
                this.http.get('https://viacep.com.br/ws/' + cep + '/json')
                    .map(dados => dados.json())
                    .subscribe(dados => this.populacampos(dados))


            } //end if.
            else {
                //cep é inválido.
                this.dadosMotoristas.cep = '';
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            this.dadosMotoristas.cep = '';
        }
    }

    populacampos(dados) {
        this.dMotoristas = dados;
        this.dadosMotoristas.cep = this.dMotoristas['cep'];
        this.dadosMotoristas.complemento = this.dMotoristas['complemento'];
        this.dadosMotoristas.endereco = this.dMotoristas['logradouro'];
        this.dadosMotoristas.numero = this.dMotoristas['numero'];
        this.dadosMotoristas.bairro = this.dMotoristas['bairro'];
        this.dadosMotoristas.cidade = this.dMotoristas['localidade'];
        this.dadosMotoristas.estado = this.dMotoristas['uf'];
    }

    

}