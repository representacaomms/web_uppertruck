import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'fotosMotorista',
    templateUrl: './fotosMotorista.html',
})
export class FotosMotorista {
    imagePathFoto:any;
    imagePathDoc:any;

    constructor(private http: Http, private route: ActivatedRoute,
        private router: Router) {
            this.route.params.subscribe(res => {
                this.buscarFotos(res.id)
            });
        ;
    }
    buscarFotos(cpf) {
        this.imagePathFoto = 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/fotos%2F' + cpf +'%2Fselfie?alt=media',
		this.imagePathDoc = 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/fotos%2F'+ cpf +'%2Fcnh?alt=media';
    }

}
