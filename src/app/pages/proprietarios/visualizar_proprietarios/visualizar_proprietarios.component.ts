import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';


@Component({
    selector: 'visualizar_proprietarios',
    templateUrl: './visualizar_proprietarios.html',
    styleUrls: ['./visualizar_proprietarios.scss']

})
export class VisualizarProprietarios {
    proprietarios: any = [];
    id: any;
    dadosProprietarios: any = {};
    dProprietarios: any = {};
    listar: any = [];

    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService,
        private router: Router) {

        this.route.params.subscribe(res => {
            this.id = res.id;
        });

    }

    ngOnInit() {
        this.getListar()
    }
    getListar() {
        let tabela = 'proprietarios'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })

    }
    populardadosCNPJ(record) {

        const dados = record;

        console.log(dados)
        this.dadosProprietarios.idProprietario = dados['idProprietario'];
        this.dadosProprietarios.cpf = dados['cpf'];
        this.dadosProprietarios.data_admi = dados['data_admi'];
        this.dadosProprietarios.nome = dados['nome'];
        this.dadosProprietarios.est_civil = dados['est_civil'];
        this.dadosProprietarios.situacao = dados['situacao'];
        this.dadosProprietarios.nome_pai = dados['nome_pai'];
        this.dadosProprietarios.nome_mae = dados['nome_mae'];
        this.dadosProprietarios.telefone1 = dados['telefone1'];
        this.dadosProprietarios.telefone2 = dados['telefone2'];
        this.dadosProprietarios.celular = dados['celular'];
        this.dadosProprietarios.email = dados['email'];
        this.dadosProprietarios.contato = dados['contato'];
        this.dadosProprietarios.dtNasc = dados['dtNasc'];
        this.dadosProprietarios.cep = dados['cep'];
        this.dadosProprietarios.complemento = dados['complemento'];
        this.dadosProprietarios.endereco = dados['endereco'];
        this.dadosProprietarios.numero = dados['numero'];
        this.dadosProprietarios.bairro = dados['bairro'];
        this.dadosProprietarios.cidade = dados['cidade'];
        this.dadosProprietarios.estado = dados['estado'];
        this.dadosProprietarios.rg = dados['rg'];
        this.dadosProprietarios.uf_rg = dados['uf_rg'];
        this.dadosProprietarios.cidade_nasc = dados['cidade_nasc'];
        this.dadosProprietarios.pais = dados['pais'];
        this.dadosProprietarios.cnh_registro = dados['cnh_registro'];
        this.dadosProprietarios.cnh_espelho = dados['cnh_espelho'];
        this.dadosProprietarios.cnh_categoria = dados['cnh_categoria'];
        this.dadosProprietarios.cnh_pri_habilitacao = dados['cnh_pri_habilitacao'];
        this.dadosProprietarios.cnh_dt_emiss = dados['cnh_dt_emiss'];
        this.dadosProprietarios.cnh_renach = dados['cnh_renach'];
        this.dadosProprietarios.ref_nome1 = dados['ref_nome1'];
        this.dadosProprietarios.ref_contato1 = dados['ref_contato1'];
        this.dadosProprietarios.ref_nome2 = dados['ref_nome2'];
        this.dadosProprietarios.ref_contato2 = dados['ref_contato2'];
    }
    atualizar() {
        this.service.makeToast('success', 'Informação!', 'Você será redirecionado!');
        setTimeout(() => {
            this.router.navigate([`pages/proprietarios/editar-proprietarios/${this.id}`])
        }, 3500);
    }
    cancelar() {
        // this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/proprietarios/listar-proprietarios`])
        }, 3500);
    }

}