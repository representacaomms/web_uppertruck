import { Component, ViewContainerRef, OnInit, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'adicionar-proprietarios',
  templateUrl: './adicionar-proprietarios.html',
  styleUrls: ['./adicionar-proprietarios.scss']
})
export class AdicionarProprietarios implements OnInit {
  dadosProprietarios: any = {};
  dProprietarios: any = {};
  proprietarios: any;
  cnpj: any;
  motorista: any;

  constructor(
    private http: Http,
    private router: Router,
    private service: DataService,
    public ds: DsService,
    @Inject(SESSION_STORAGE) private storage: StorageService
  ) {}

  ngOnInit() {
    this.dadosProprietarios.data_admi = this.service.pegarData();

    let tabela = 'motoristas';
    this.service.getAllDoc(tabela).subscribe(result => {
      const resultado = [];
      for (const key in result) {
        result[key].id = key;
        resultado.push(result[key]);
      }
      this.motorista = resultado;
    });
  }

  buscarCPF(cpf) {
    // tslint:disable-next-line: no-console
    console.log(cpf);
    for (const key in this.motorista) {
      console.log(this.motorista[key].cpf);

      if (this.motorista[key].cpf === cpf) {
        this.populardadosCNPJ(this.motorista[key]);
      }
    }
  }

  buscarDados(e) {
    console.log(e);
    //Nova variável 'cep' somente com dígitos.
    var cep = e.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != '') {
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if (validacep.test(cep)) {
        //Sincroniza com o callback.
        this.http
          .get('https://viacep.com.br/ws/' + cep + '/json')
          .map(dados => dados.json())
          .subscribe(dados => this.populacampos(dados));
      } //end if.
      else {
        //cep é inválido.
        this.dadosProprietarios.cep = '';
        alert('Formato de CEP inválido.');
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      this.dadosProprietarios.cep = '';
    }
  }

  populacampos(dados) {
    this.dProprietarios = dados;
    this.dadosProprietarios.cep = this.dProprietarios['cep'];
    this.dadosProprietarios.complemento = this.dProprietarios['complemento'];
    this.dadosProprietarios.endereco = this.dProprietarios['logradouro'];
    this.dadosProprietarios.numero = this.dProprietarios['numero'];
    this.dadosProprietarios.bairro = this.dProprietarios['bairro'];
    this.dadosProprietarios.cidade = this.dProprietarios['localidade'];
    this.dadosProprietarios.estado = this.dProprietarios['uf'];
  }

  cadastrar(value) {
    console.log(value);
    let tabela = 'proprietarios';
    this.service.createDoc(value, tabela).subscribe(res => {
      this.service.makeToast(
        'success',
        'Concluido!',
        'Cadastrado com sucesso!'
      );
      setTimeout(() => {
        this.router.navigate([`pages/proprietarios/listar-proprietarios`]);
      }, 3000);
    });
  }

  populardadosCNPJ(record) {
    const dados = record;

    console.log(dados);
    this.dadosProprietarios.idProprietario = dados['idProprietario'];
    this.dadosProprietarios.cpf = dados['cpf'];
    this.dadosProprietarios.data_admi = dados['data_admi'];
    this.dadosProprietarios.nome = dados['nome'];
    this.dadosProprietarios.est_civil = dados['est_civil'];
    this.dadosProprietarios.situacao = dados['situacao'];
    this.dadosProprietarios.nome_pai = dados['nome_pai'];
    this.dadosProprietarios.nome_mae = dados['nome_mae'];
    this.dadosProprietarios.telefone1 = dados['telefone1'];
    this.dadosProprietarios.telefone2 = dados['telefone2'];
    this.dadosProprietarios.celular = dados['celular'];
    this.dadosProprietarios.email = dados['email'];
    this.dadosProprietarios.contato = dados['contato'];
    this.dadosProprietarios.dtNasc = dados['dtNasc'];
    this.dadosProprietarios.cep = dados['cep'];
    this.dadosProprietarios.complemento = dados['complemento'];
    this.dadosProprietarios.endereco = dados['endereco'];
    this.dadosProprietarios.numero = dados['numero'];
    this.dadosProprietarios.bairro = dados['bairro'];
    this.dadosProprietarios.cidade = dados['cidade'];
    this.dadosProprietarios.estado = dados['estado'];
    this.dadosProprietarios.rg = dados['rg'];
    this.dadosProprietarios.uf_rg = dados['uf_rg'];
    this.dadosProprietarios.cidade_nasc = dados['cidade_nasc'];
    this.dadosProprietarios.pais = dados['pais'];
    this.dadosProprietarios.cnh_registro = dados['cnh_registro'];
    this.dadosProprietarios.cnh_espelho = dados['cnh_espelho'];
    this.dadosProprietarios.cnh_categoria = dados['cnh_categoria'];
    this.dadosProprietarios.cnh_pri_habilitacao = dados['cnh_pri_habilitacao'];
    this.dadosProprietarios.cnh_dt_emiss = dados['cnh_dt_emiss'];
    this.dadosProprietarios.cnh_renach = dados['cnh_renach'];
    this.dadosProprietarios.ref_nome1 = dados['ref_nome1'];
    this.dadosProprietarios.ref_contato1 = dados['ref_contato1'];
    this.dadosProprietarios.ref_nome2 = dados['ref_nome2'];
    this.dadosProprietarios.ref_contato2 = dados['ref_contato2'];
  }

  cancelar() {
    this.service.makeToast('info', 'Concluido!', 'Você será redirecionado!');
    setTimeout(() => {
      this.router.navigate([`pages/proprietarios/listar-proprietarios`]);
    }, 3000);
  }
}
