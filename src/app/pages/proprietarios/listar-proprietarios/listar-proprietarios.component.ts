import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

@Component({
    selector: 'app-listar-proprietarios',
    templateUrl: './listar-proprietarios.component.html',
    styleUrls: ['./listar-proprietarios.component.scss']
})
export class ListarProprietariosComponent implements OnInit {


    proprietarios: any = [];
    listar: any = [];
    getProprietarios: any = [];
    imagePath: any;
    cnpj: any;

    constructor(private http: Http, private router: Router,  public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService) {
        this.imagePath = 'https://firebasestorage.googleapis.com/v0/b/apprastreamento-2c88f.appspot.com/o/fotos%2F5afb4da872d5b20004284ec2%2Fselfie?alt=media&token=c3e24909-9c0e-471a-bb0e-74c2f14e2197';

    }

    ngOnInit() {        
		this.getListar();	   

    }

    adicionar() {
        this.router.navigate([`pages/proprietarios/adicionar-proprietarios`])
    }
    visualizarDados(message) {
        this.router.navigate([`pages/proprietarios/visualizar-proprietarios/${message.id}`])
    }
    editarDados(message) {
        this.router.navigate([`pages/proprietarios/editar-proprietarios/${message.id}`])

    }
    getItems(ev) {
        // Reset items back to all of the items
        this.proprietarios;

        // set val to the value of the ev target
        var val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.proprietarios = this.proprietarios.filter((item) => {
                return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.ngOnInit();
        }
    }


    getListar() {
		let tabela =   'proprietarios';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.listar = resultado			
		}) 
		
	}
    excluirDados() {

    }

    fotos() {
        this.router.navigate([`pages/proprietarios/fotos-proprietarios`])
    }

}
