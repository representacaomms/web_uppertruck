import { Routes, RouterModule }  from '@angular/router';


import { Proprietarios } from './proprietarios';
import { ListarProprietariosComponent } from './listar-proprietarios/listar-proprietarios.component';
import { AdicionarProprietarios } from './adicionar-proprietarios';
import { EditarProprietarios } from './editar_proprietarios';
import { VisualizarProprietarios } from './visualizar_proprietarios';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Proprietarios,
    children: [
       { path: 'listar-proprietarios', component: ListarProprietariosComponent}, 
       { path: 'adicionar-proprietarios', component: AdicionarProprietarios}, 
       { path: 'editar-proprietarios/:id', component: EditarProprietarios}, 
       { path: 'visualizar-proprietarios/:id', component: VisualizarProprietarios}, 

    ]
  }
];

export const routing = RouterModule.forChild(routes);
