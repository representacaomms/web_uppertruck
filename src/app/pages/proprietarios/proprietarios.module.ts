import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './proprietarios.routing';

import { Proprietarios } from './proprietarios';
import { ListarProprietariosComponent } from './listar-proprietarios/listar-proprietarios.component';
import { AdicionarProprietarios } from './adicionar-proprietarios';
import { EditarProprietarios } from './editar_proprietarios';
import { VisualizarProprietarios } from './visualizar_proprietarios';
import { ThemeModule } from '../../@theme/theme.module';

import {ToastModule} from 'primeng/toast';
import { InputMaskModule } from 'primeng/inputmask';





@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule,
    ToastModule,
    InputMaskModule
  ],
  declarations: [
    Proprietarios,
    ListarProprietariosComponent,
    AdicionarProprietarios,
    EditarProprietarios,
    VisualizarProprietarios
  ]
})
export class ProprietariosModule {
}
