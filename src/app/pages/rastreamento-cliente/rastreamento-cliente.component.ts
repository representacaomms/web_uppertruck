import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { DataService } from '../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { HttpClient } from '@angular/common/http';


declare var google: any;

@Component({
  selector: 'ngx-rastreamento-cliente',
  templateUrl: './rastreamento-cliente.component.html',
  styleUrls: ['./rastreamento-cliente.component.scss']
})
export class RastreamentoClienteComponent implements OnInit {


  latitude: number;
  longitude: number;
  isTracking: any;
  currentLat: any;
  currentLong: any;
  marker: any;
  key = 'AIzaSyAVwITJ9kcs9RbeuKgpTZI1Qd39q51Nv04';
  pesquisa: any;
  posicao: any = [];
  posicaoEntrega: any;
  lng: any;
  lat: any;
  entregas: any = [];
  coletas: any = [];
  @ViewChild('gmap') gmapElement: any;
  map: any; // google.maps.Map;
  id_motorista = '5afb4da872d5b20004284ec2';
  romaneio_lat: any;
  romaneio_lng: any;
  posicaoCaminhao: any = [];
  romaneio: any = {};
  client: any;
  driver: any = [];
  latPosition: any;
  lngPosition: any;
  listDriver: any = {};
  updateMapPointer: any;
  coordenadas: any;

  listar: any = {};
  cnpj: any;

  array_Motoristas: any = [];

  data: any;
  markers = [];

  image = {
    // tslint:disable-next-line: max-line-length
    url: 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/mascote.png?alt=media&token=0674dab3-7b0b-4075-88ac-9a6718afcd31',
  }

  // tslint:disable-next-line: max-line-length
  constructor(public service: DataService, @Inject(SESSION_STORAGE) private storage: StorageService, public db: AngularFireDatabase, private http: HttpClient) {
    this.cnpj = this.storage.get('cnpj')

  }

  ngOnInit() {
    const refRomaneio = this.db.database.ref('romaneios');
    refRomaneio.on('child_added', snapshot => {
      const value = snapshot.val();
      if (value.coleta.cnpj_cliente === this.cnpj) {
        let dadosMotor = {cpf: value.data.cpf, nome: value.data.nome, placa: value.data.placa}
        this.array_Motoristas.push(dadosMotor);
      }
      if (value.entrega.cnpj_cliente === this.cnpj) {
        let dadosMotor = {cpf: value.data.cpf, nome: value.data.nome, placa: value.data.placa}
        this.array_Motoristas.push(dadosMotor);
      }
    });
    setTimeout(() => {
      this.openMapa();
    }, 1000);

  }
  openMapa() {
    console.log(this.array_Motoristas)
    var markers = [];

    var image = {
      // tslint:disable-next-line: max-line-length
      url: 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/mascote.png?alt=media&token=0674dab3-7b0b-4075-88ac-9a6718afcd31',
    }
    var mudancaPos: any = [];
    const iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    var markers = [];

    navigator.geolocation.getCurrentPosition((coords) => {
      this.posicao = new google.maps.LatLng(coords['coords'].latitude, coords['coords'].longitude);

      const mapOptions = {
        center: this.posicao,
        zoom: 6,

      };



      const map = new google.maps.Map(this.gmapElement.nativeElement, mapOptions);
      var dbRef = this.db.database.ref('geolocations');
      for (let index = 0; index < this.array_Motoristas.length; index++) {
        const element = this.array_Motoristas[index];
        dbRef.on('child_added', function (data) {
          const record = data.val();
          if (record.cpf === element.cpf) {
            var uluru = { lat: data.val().latitude, lng: data.val().longitude };
            var marker = new google.maps.Marker({
              position: uluru,
              // Animção
              animation: google.maps.Animation.ROUND, // BOUNCE
              // Icone
              icon: image,
              map: map,
              title: `Nome:${element.nome}\nEspaço Caminhão: Carga na Metade\nPlaca: ${element.placa}`,
            });
            markers[data.key] = marker;
          }
        });

        dbRef.on('child_changed', function (data) {
          //markers[data.key].setMap(null);
          const record = data.val();
          if (record.cpf === element.cpf) {
            var uluru = { lat: data.val().latitude, lng: data.val().longitude };
            var marker = new google.maps.Marker({
              position: uluru,
              // Animção
              animation: google.maps.Animation.ROUND, // BOUNCE
              // Icone
              icon: image,
              map: map,
              title: `Nome:${element.nome}\nEspaço Caminhão: Carga na Metade\nPlaca: ${element.placa}`,

            });
            markers[data.key] = marker;
          }
        });

      }



    });
  }



}
