import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RastreamentoClienteComponent } from './rastreamento-cliente.component';

describe('RastreamentoClienteComponent', () => {
  let component: RastreamentoClienteComponent;
  let fixture: ComponentFixture<RastreamentoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RastreamentoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RastreamentoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
