import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './fornecedores.routing';

import { Fornecedores } from './fornecedores';
import { AdicionarFornecedores } from './adicionar-fornecedores';
import { ListarFornecedoresComponent } from './listar-fornecedores/listar-fornecedores.component';
import { EditarFornecedores } from './editar_fornecedores';
import { VisualizarFornecedores } from './visualizar_fornecedores';

import { ThemeModule } from '../../@theme/theme.module';
import { ToastModule } from 'primeng/toast';




@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule,
    ToastModule
  ],
  declarations: [
    Fornecedores,
    AdicionarFornecedores,
    ListarFornecedoresComponent,
    EditarFornecedores,
    VisualizarFornecedores
  ]
})
export class FornecedoresModule {
}
