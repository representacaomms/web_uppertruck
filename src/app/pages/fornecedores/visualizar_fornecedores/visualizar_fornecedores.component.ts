import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'visualizar_fornecedores',
    templateUrl: './visualizar_fornecedores.html',
})
export class VisualizarFornecedores {
    id:any;
    dadosFornecedores: any = {};
    dFornecedores: any = {};
    api='https://servidor-notafacil.herokuapp.com/api/';

    constructor(private http: Http, private route: ActivatedRoute, private router: Router) {
        this.route.params.subscribe(res => this.id = res.id);
        this.buscarDados(this.id);
    }
    buscarDados(e) {
        this.http.get(`${this.api}fornecedores/${e}`)
            .map(dados => dados.json())
            .subscribe(dados => this.populardadosCNPJ(dados));
    }
    populardadosCNPJ(dados) {
        console.log(dados)
        this.dadosFornecedores.cnpj = dados['cnpj'];
        this.dadosFornecedores.razao_social = dados['razao_social'];
        this.dadosFornecedores.fantasia = dados['fantasia'];
        this.dadosFornecedores.telefone = dados['telefone'];
        this.dadosFornecedores.email = dados['email'];
        this.dadosFornecedores.cep = dados['cep'];
        this.dadosFornecedores.complemento = dados['complemento'];
        this.dadosFornecedores.endereco = dados['endereco'];
        this.dadosFornecedores.numero = dados['numero'];
        this.dadosFornecedores.bairro = dados['bairro'];
        this.dadosFornecedores.cidade = dados['cidade'];
        this.dadosFornecedores.estado = dados['estado'];
        this.dadosFornecedores.segmento = dados['segmento'];
        this.dadosFornecedores.celular = dados['celular'];
        this.dadosFornecedores.data_cadastro = dados['data_cadastro'];
        this.dadosFornecedores.dtNasc = dados['dtNasc'];
        this.dadosFornecedores.insc_est = dados['insc_est'];
        this.dadosFornecedores.insc_mun = dados['insc_mun'];
        this.dadosFornecedores.contato = dados['contato'];

    }
    atualizar() {
        //this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/crud/editarFornecedores/${this.id}`])
        }, 3500);
    }
    cancelar() {
        //this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/crud/listarFornecedores`])
        }, 3500);
    }

}
