import { Component, ViewContainerRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    selector: 'adicionar-fornecedores',
    templateUrl: './adicionar-fornecedores.html',
})
export class AdicionarFornecedores implements OnInit {
    dadosFornecedores: any = {};
    dFornecedores: any = {};

    constructor(private http: Http,  private router: Router) {
    }

    ngOnInit(){}

    buscarDados(e) {
        let cnpj = e.replace(/\D/g, '');
        this.http.get(`https://servidor-notafacil.herokuapp.com/api/cnpj/${cnpj}`)
        .subscribe(dados => {
            let data = JSON.parse(dados['_body'])
                if (data.status == "ERROR") {
                }
                this.populardadosCNPJ(data);
        })     

    }
    populardadosCNPJ(dados) {
        this.dFornecedores = dados;
        this.dadosFornecedores.razao_social = this.dFornecedores['nome'];
        this.dadosFornecedores.fantasia = this.dFornecedores['fantasia'];
        this.dadosFornecedores.telefone = this.dFornecedores['telefone'];
        this.dadosFornecedores.email = this.dFornecedores['email'];
        this.dadosFornecedores.cep = this.dFornecedores['cep'];
        this.dadosFornecedores.complemento = this.dFornecedores['complemento'];
        this.dadosFornecedores.endereco = this.dFornecedores['logradouro'];
        this.dadosFornecedores.numero = this.dFornecedores['numero'];
        this.dadosFornecedores.bairro = this.dFornecedores['bairro'];
        this.dadosFornecedores.cidade = this.dFornecedores['municipio'];
        this.dadosFornecedores.estado = this.dFornecedores['uf'];
        this.dadosFornecedores.segmento = this.dFornecedores['atividade_principal'][0]['text'];
        this.dadosFornecedores.dtNasc = this.dFornecedores['abertura'];

    }
    cadastrar(dados) {
        this.http.post(`https://servidor-notafacil.herokuapp.com/api/fornecedores/`, dados)
            .subscribe(dados => {
                let data = JSON.parse(dados['_body']);
                this.router.navigate([`pages/crud/listarFornecedores`]);
            })
    }


}
