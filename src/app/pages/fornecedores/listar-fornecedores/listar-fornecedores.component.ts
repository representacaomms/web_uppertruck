import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';

@Component({
  selector: 'app-listar-fornecedores',
  templateUrl: './listar-fornecedores.component.html',
  styleUrls: ['./listar-fornecedores.component.scss']
})
export class ListarFornecedoresComponent implements OnInit {


getFornecedores:any=[];

  constructor(private http: Http, private router: Router, private service: DataService) { 
  }

  ngOnInit() {
   this.getListar();
  }

  visualizarDados(message){
    this.router.navigate([`pages/crud/visualizarFornecedores/${message.id}`])
  }
  editarDados(message){
    this.router.navigate([`pages/crud/editarFornecedores/${message.id}`])

  }
  getListar() {
		let tabela =  'fornecedores';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.getFornecedores = resultado			
		}) 
		
	}
  

  getItems(ev) {
     // Reset items back to all of the items
     this.getFornecedores;

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.getFornecedores = this.getFornecedores.filter((item) => {
        return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      this.ngOnInit();
    }
  }

}
