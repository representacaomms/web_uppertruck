import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { docChanges } from '@angular/fire/firestore';

@Component({
  selector: 'ngx-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {
  getRelatoriosDoc: any = [];
  getRelatoriosCnh: any = [];
  relatorio: any = {};
  tipo: any;
  data_inicial:any;
  data_final:any;

  constructor(public service: DataService) { }

  ngOnInit() {
    this.getListar();
    this.tipo = 'cnh'
  }

  getListar() {
    let tabela = 'motoristas';
    this.service.getAllDoc(tabela).subscribe(result => {
      const resultado = [];
      for (const key in result) {
        result[key].id = key;
        resultado.push(result[key])
      }
      this.getRelatoriosCnh = resultado
    });
    let tabela2 = 'veiculos';
    this.service.getAllDoc(tabela2).subscribe(result2 => {
      const resultado = [];
      for (const key in result2) {
        result2[key].id = key;
        resultado.push(result2[key])
      }
      this.getRelatoriosDoc = resultado
    });

    setTimeout(() => {
      console.log(this.getRelatoriosCnh, this.getRelatoriosDoc)

    }, 500);

  }

  pesquisar(data_inicial, data_final, tipo) {
    console.log(tipo)
    if (tipo === 'doc'){
      const tabela = this.getRelatoriosDoc;
      let start = data_inicial;
      let end = data_final;
  
      let selectedMembers = tabela.filter(
        m => new Date(m.cnh_dt_emiss) >= new Date(start) && new Date(m.cnh_dt_emiss) <= new Date(end)
      );
  
      this.getRelatoriosDoc = selectedMembers;
  
    } else {
      const tabela = this.getRelatoriosCnh;
      let start = data_inicial;
      let end = data_final;
  
      let selectedMembers = tabela.filter(
        m => new Date(m.cnh_dt_emiss) >= new Date(start) && new Date(m.cnh_dt_emiss) <= new Date(end)
      );  
      this.getRelatoriosCnh = selectedMembers;
  
    }

   
  }





}
