import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanhotosComponent } from './canhotos.component';

describe('CanhotosComponent', () => {
  let component: CanhotosComponent;
  let fixture: ComponentFixture<CanhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
