import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { OverlayPanel } from 'primeng/overlaypanel';

@Component({
  selector: 'ngx-canhotos',
  templateUrl: './canhotos.component.html',
  styleUrls: ['./canhotos.component.scss']
})
export class CanhotosComponent implements OnInit {
  link: any = [];
  linkSelecionado: any;
  listar: any = [];

  constructor(private servive: DataService) { }

  ngOnInit() {
    this.getListar();
    this.link = this.servive.getAllFotos();
  }

  ver(event,value, overlaypanel: OverlayPanel){
    console.log(value)
     this.linkSelecionado = value;
    overlaypanel.toggle(event); 
  }

  getItems(event){
    console.log(event)
  }

  getListar() {
		let tabela =   'clientes';
		this.servive.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.listar = resultado			
    }) 
    
    console.log(this.listar)
		
	}
 

}
