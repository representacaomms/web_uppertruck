import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { RepresentantesModule } from './representantes/representantes.module';
import { DataService } from '../data.service';
import { ClientesModule } from './clientes/clientes.module';
import { FornecedoresModule } from './fornecedores/fornecedores.module';
import { FretesModule } from './fretes/fretes.module';
import { MotoristasModule } from './motoristas/motoristas.module';
import { RomaneioModule } from './romaneio/romaneio.module';
import { ProprietariosModule } from './proprietarios/proprietarios.module';
import { VeiculosModule } from './veiculos/veiculos.module';
import { ToastModule } from 'primeng/toast';
import { CanhotosComponent } from './canhotos/canhotos.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';

import {OverlayPanelModule} from 'primeng/overlaypanel';

const PAGES_COMPONENTS = [
  PagesComponent,
  CanhotosComponent,
  RelatoriosComponent
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    RepresentantesModule,
    ClientesModule,
    FornecedoresModule,
    FretesModule,
    MotoristasModule,
    RomaneioModule,
    ProprietariosModule,
    VeiculosModule,
    ToastModule,
    OverlayPanelModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    RelatoriosComponent,
  ],
})
export class PagesModule {
}
