import { Component, ViewContainerRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
declare var deepstream: any;

@Component({
    selector: 'adicionar',
    templateUrl: './adicionar.html',
    styleUrls: ['./adicionar.component.scss']
})
export class Adicionar implements OnInit {
    dadosClientes: any = {};
    dClientes: any = {};
    value = '';
    eventsReceived = [];

    getClientes: any = [];
    record: any = [];
    firstname: any;
    lastname: any;

    title = 'app works!';
    userStatus = "";
    user = "";
    timeline = [];
    clientes: any;


    constructor(private http: Http, private router: Router, private service: DataService, public ds: DsService) {
    }

    ngOnInit() {
        this.clientes = "this.ds.ds.login()";    
      
    }


    buscarDados($e) {
        if ($e < 13) {
            this.service.makeToast('info', 'Informação!', 'CNPJ Invalido, apenas numero');
        } else {
           
            const dados =  $e.replace(/\D/g,'');
            let cnpj = { cnpj: dados}
             this.service.buscarCNPJ(JSON.stringify(cnpj)).subscribe(data => {
                let dados = data;
                this.populardadosCNPJ(dados)
            }) 

        }

    }
    populardadosCNPJ(dados) {
        this.dClientes = dados;
        this.dadosClientes.razao_social = this.dClientes['nome'];
        this.dadosClientes.fantasia = this.dClientes['fantasia'];
        this.dadosClientes.telefone = this.dClientes['telefone'];
        this.dadosClientes.email = this.dClientes['email'];
        this.dadosClientes.cep = this.dClientes['cep'];
        this.dadosClientes.complemento = this.dClientes['complemento'];
        this.dadosClientes.endereco = this.dClientes['logradouro'];
        this.dadosClientes.numero = this.dClientes['numero'];
        this.dadosClientes.bairro = this.dClientes['bairro'];
        this.dadosClientes.cidade = this.dClientes['municipio'];
        this.dadosClientes.estado = this.dClientes['uf'];
        this.dadosClientes.segmento = this.dClientes['atividade_principal'][0]['text'];
        this.dadosClientes.dtNasc = this.dClientes['abertura'];

    }
    cadastrar(dados) {   

        console.log(dados)
        var idCliente = this.clientes.getUid();
        var recordName = `tb_representantes/` + idCliente; //create a unique record name
        var record = this.clientes.record.getRecord(recordName);
        record.set({     
            "idCliente": idCliente,           
            "bairro": dados.bairro,
            "celular": dados.celular,
            "cep": dados.cep,
            "cidade": dados.cidade,
            "cnpj":  dados.cnpj,
            "complemento": dados.complemento,
            "contato": dados.contato,
            "data_cadastro": dados.data_cadastro,
            "dtNasc": dados.dtNasc,
            "email": dados.email,
            "endereco": dados.endereco,
            "estado": dados.estado,
            "fantasia": dados.fantasia,
            "insc_est": dados.insc_est,
            "insc_mun": dados.insc_mun,
            "numero": dados.numero,
            "razao_social": dados.razao_social,
            "segmento": dados.segmento,
            "telefone": dados.telefone,
            "password": dados.password,
            "credenciais": dados.credenciais

        }); 
        
            const list = this.clientes.record.getList('tb_representantes');
            list.addEntry(recordName);
        
        this.service.makeToast('success', 'Sucesso!', 'Cadastrado com sucesso!');
        this.router.navigate([`/pages/representantes/listar`]); 

    }
    buscarCep($e) {
        this.service.buscarCep($e).subscribe(data => {
            console.log(data)
        });
    }

   
}



