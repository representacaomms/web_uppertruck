import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './represetantes.routing';

import { RepresentantesComponent } from './representantes.component';
import { RepresentantesService } from './representantes.service';
import { ListarComponent } from './listar/listar.component';
import { DataService } from '../../data.service';
import { Adicionar } from './adicionar';

import { ThemeModule } from '../../@theme/theme.module';
import { Editar } from './editar';





@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    ThemeModule,
    routing,
    HttpModule
  ],
  declarations: [
    RepresentantesComponent,
    ListarComponent,
    Adicionar,
    Editar
  ],
  providers:[RepresentantesService, DataService]
})
export class RepresentantesModule {
}
