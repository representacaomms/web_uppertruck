import { Routes, RouterModule }  from '@angular/router';


import { RepresentantesComponent } from './representantes.component';
import { ListarComponent } from './listar/listar.component';
import { Adicionar } from './adicionar';
import { Editar } from './editar';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: RepresentantesComponent,
    children: [
      { path: 'listar', component: ListarComponent},
      { path: 'adicionar', component: Adicionar}, 
      { path: 'editar/:id', component: Editar}, 

    ]
  }
];

export const routing = RouterModule.forChild(routes);
