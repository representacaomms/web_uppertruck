import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DsService } from '../../../ds.service';
import { DataService } from '../../../data.service';

@Component({
	selector: 'app-listar',
	templateUrl: './listar.component.html',
	styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

	clientes: any = [];
	listar: any = [];

	constructor(private http: Http, private router: Router, public service: DataService, public ds: DsService) {
	}

	ngOnInit() {

		this.clientes ="this.ds.ds.login()"
		this.getListar();

	/* 	this.service.event.subscribe('cadastro', data => {
			this.getListar()
		}, true); */

	}

	adicionar() {
		this.router.navigate([`pages/representantes/adicionar`])
	}
	editarDados(message) {
		this.router.navigate([`pages/representantes/editar/${message.idCliente}`])

	}
	getItems(ev) {
		// Reset items back to all of the items
		this.clientes;

		// set val to the value of the ev target
		var val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.clientes = this.clientes.filter((item) => {
				return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		} else {
			this.ngOnInit();
		}
	}


	getListar() {
        const todos = this.clientes.record.getList('tb_representantes' );     //todos.delete()
        todos.whenReady(list => {
            /** Fetch all existing entries */
            const entries = list.getEntries();

            for (let i = 0; i < entries.length; i++) {
                this.clientes.record.getRecord(entries[i]).whenReady(record => {
                     record.subscribe(data => {
                        this.listar.unshift(data);
                    }, true);
                });
            }

        });
    }
	excluirDados(dados) {

	}

}
