import { Component, ViewContainerRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
declare var deepstream: any;

@Component({
    selector: 'editar',
    templateUrl: './editar.html',
    styleUrls: ['./editar.component.scss']
})
export class Editar implements OnInit {
    

clientes: any = [];
id: any;
dadosClientes: any = {};
dClientes: any = {};
listar: any = [];

constructor(private http: Http, private route: ActivatedRoute,  public service: DataService, public ds: DsService,
    private router: Router) {

    this.route.params.subscribe(res => {
        this.id = res.id;            
    });

}

ngOnInit() {

    this.clientes = "this.ds.ds.login()";

    this.getListar()

   


}
getListar() {

   
}
populardadosCNPJ(record) {
    const dados = record;
    this.dadosClientes.cnpj = dados['cnpj'];
    this.dadosClientes.razao_social = dados['razao_social'];
    this.dadosClientes.fantasia = dados['fantasia'];
    this.dadosClientes.telefone = dados['telefone'];
    this.dadosClientes.email = dados['email'];
    this.dadosClientes.cep = dados['cep'];
    this.dadosClientes.complemento = dados['complemento'];
    this.dadosClientes.endereco = dados['endereco'];
    this.dadosClientes.numero = dados['numero'];
    this.dadosClientes.bairro = dados['bairro'];
    this.dadosClientes.cidade = dados['cidade'];
    this.dadosClientes.estado = dados['estado'];
    this.dadosClientes.segmento = dados['segmento'];
    this.dadosClientes.celular = dados['celular'];
    this.dadosClientes.data_cadastro = dados['data_cadastro'];
    this.dadosClientes.dtNasc = dados['dtNasc'];
    this.dadosClientes.insc_est = dados['insc_est'];
    this.dadosClientes.insc_mun = dados['insc_mun'];
    this.dadosClientes.contato = dados['contato'];
    this.dadosClientes.password = dados['password'];
    this.dadosClientes.credenciais = dados['credenciais'];

}
atualizar(dados) {
    const cliente = this.clientes.record.getRecord(`tb_representantes/${this.id}`);  // todos.delete()

    cliente.set('fantasia', dados['fantasia']);
    cliente.set('telefone', dados['telefone']);
    cliente.set('email', dados['email']);
    cliente.set('cep', dados['cep']);
    cliente.set('complemento', dados['complemento']);
    cliente.set('endereco', dados['endereco']);
    cliente.set('numero', dados['numero']);
    cliente.set('bairro', dados['bairro']);
    cliente.set('cidade', dados['cidade']);
    cliente.set('estado', dados['estado']);
    cliente.set('segmento', dados['segmento']);
    cliente.set('celular', dados['celular']);
    cliente.set('dtNasc', dados['dtNasc']);
    cliente.set('contato', dados['contato']);
    cliente.set('status', dados['status']);
    cliente.set('password', dados['password']);
    cliente.set('credenciais', dados['credenciais']);

    //this.toastr.success('Cliente editando com sucesso! </br> Você será redirecionado', 'Informação!');
    setTimeout(() => {
        this.router.navigate([`pages/representantes/listar`])
    }, 3500);

}
cancelar() {
    //this.toastr.info('Você será redirecionado', 'Informação!');
    setTimeout(() => {
        this.router.navigate([`pages/representantes/listar`])
    }, 3500);
}

}



