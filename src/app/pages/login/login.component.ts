import { Router, CanDeactivate } from '@angular/router';
import {
  Component,
  OnInit,
  Inject,
  ViewContainerRef,
  HostListener,
  OnDestroy
} from '@angular/core';
import {
  FormGroup,
  AbstractControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { DsService } from '../../ds.service';
import { DataService } from '../../data.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AuthGuard } from '../../guards/auth-guard';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  data: any = {};
  tb_representantes: any;
  tb_logados: any;
  user: any = [];
  userLogados: any = [];
  formLogin: FormGroup;
  usuario: any;
  password: any;
  listar: any;
  login: any;
  index: any;
  status: any;
  credenciais: any = [];
  userLog: any = [];
  IP: any;
  cnpj: any;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthGuard,
    public ds: DsService,
    private service: DataService,
    @Inject(SESSION_STORAGE) private storage: StorageService
  ) {
    this.getListar();
  }

// tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit() {
    if (this.storage.get('status') === true) {
      this.router.navigate(['/pages']);
    }
  }
  ngOnInit() {
    this.data.cnpj = '08171700000150';
    this.data.passworf = '081717';
    this.data.tipo_acesso = 'Cliente';
    this.formLogin = this.formBuilder.group({
      cnpj: ['', Validators.required],
      password: ['', Validators.required],
      empresa: [''],
      tipo_acesso: ['']
    });
  }

  acessar(data) {
    if (
      data.cnpj === '24940831000101' &&
      data.password === '2019@Upper12345#' &&
      data.tipo_acesso === 'Administrador'
    ) {
      this.service.makeToast(
        'sucess',
        'Acesso Permitido',
        'Acessando painel administração'
      );
      this.gerarToken(data);
      setTimeout(() => {
        this.router.navigate(['pages/iot-dashboard']);
      }, 1000);
    } else {
      this.verificarCliente(data);
    }
  }

  gerarToken(data) {
    var timestamp = new Date().getTime();
    const token = 13 + timestamp + data.cnpj + data.password;
    this.storage.set('token', token);
    this.authUsuario(true);
  }

  getListar() {
    let tabela = 'clientes';
    this.service.getAllDoc(tabela).subscribe(result => {
      const resultado = [];
// tslint:disable-next-line: forin
      for (const key in result) {
        result[key].id = key;
        resultado.push(result[key]);
      }
      this.listar = resultado;
    });
  }

  verificarCliente(data) {
    var index = this.listar.findIndex(function(it, i) {
      return it.cnpj === data.cnpj;
    });
    if (
      data.cnpj === this.listar[index].cnpj &&
      data.password === this.listar[index].senha &&
      data.tipo_acesso === 'Cliente'
    ) {
      this.gerarToken(data);
      setTimeout(() => {
        this.storage.set('cnpj', data.cnpj);
        this.router.navigate(['cliente-rastreamento']);
      }, 1000);
    } else {
      this.service.makeToast('error', 'Acesso Restrito', 'Senha incorreta');
    }
  }

  authUsuario(dados) {
    this.service.loginAuth(dados);
  }
}
