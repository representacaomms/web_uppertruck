import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
      <span>{{ modalHeader }}</span>
      
    </div>
    <div class="modal-body">
      {{ modalContent }}
    </div>
    <div class="modal-footer">
      <button class="btn btn-md btn-primary" (click)="proprietario()">Sim </button>
      <button class="btn btn-md btn-danger" (click)="motorista()">Não</button>
    </div>
  `,
})
export class ModalComponent {

  modalHeader: string;
  modalContent = ` Esse Motorista é Proprietário?`;

  constructor(private activeModal: NgbActiveModal) { }

  proprietario() {
    console.log('PROPRIETARIO')
    //this.activeModal.close();
  }
  motorista() {
    console.log('MOTORISTA')
   // this.activeModal.close();
  }
}
