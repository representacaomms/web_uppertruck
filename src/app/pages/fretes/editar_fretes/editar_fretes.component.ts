import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'editar_fretes',
    templateUrl: './editar_fretes.html',
})
export class EditarFretes {
    id: any;
    dadosFretes: any = {};
    dFretes: any = {};
    api = 'https://servidor-notafacil.herokuapp.com/api/';

    constructor(private http: Http, private route: ActivatedRoute,
        private router: Router) {
        this.route.params.subscribe(res => this.id = res.id);
        this.buscarDadosFrete(this.id);
    }
    buscarDadosFrete(e) {
        this.http.get(`${this.api}fretes/${e}`)
            .map(dados => dados.json())
            .subscribe(dados => this.populardadosFrete(dados));
    }

    populardadosFrete(dados) {
        this.dFretes = dados;
        this.dadosFretes.nomeCliente = this.dFretes['nomeCliente'];
        this.dadosFretes.nomeContato = this.dFretes['nomeContato'];
        this.dadosFretes.telContato = this.dFretes['telContato'];
        this.dadosFretes.localColeta = this.dFretes['localColeta'];
        this.dadosFretes.localEntrega = this.dFretes['localEntrega'];
        this.dadosFretes.tipodeMercadoria = this.dFretes['tipodeMercadoria'];
        this.dadosFretes.volume = this.dFretes['volume'];
        this.dadosFretes.quantidade = this.dFretes['quantidade'];
        this.dadosFretes.cubagem = this.dFretes['cubagem'];
        this.dadosFretes.peso = this.dFretes['peso'];
        this.dadosFretes.valorFrete = this.dFretes['valorFrete'];
        this.dadosFretes.status = this.dFretes['status'];
    }   

    atualizar(dados) {
        console.log(dados)
        this.http.put(`${this.api}fretes/${this.id}`, dados).subscribe(dados => {
            let data = JSON.parse(dados['_body']);
            //this.toastr.success(data.mensagem, 'Concluido!');
            setTimeout(() => {
                this.router.navigate([`pages/crud/listarFretes`])
            }, 3500);        })
    }

    cancelar() {
       // this.toastr.info('Você será redirecionado', 'Informação!');
        setTimeout(() => {
            this.router.navigate([`pages/crud/listarFretes`])
        }, 3500);
    }


}
