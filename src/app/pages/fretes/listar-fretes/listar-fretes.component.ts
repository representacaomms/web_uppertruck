import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

@Component({
	selector: 'app-listar-fretes',
	templateUrl: './listar-fretes.component.html',
	styleUrls: ['./listar-fretes.component.scss']
})
export class ListarFretesComponent implements OnInit {

	api = 'https://servidor-notafacil.herokuapp.com/api/';
	//https://servidor-notafacil.herokuapp.com/api/veiculos
	getFretes: any = [];

	constructor(private http: Http, private router: Router) {
	}

	ngOnInit() {
		this.http.get(this.api + 'fretes')
			.subscribe(dados => {
				this.getFretes = JSON.parse(dados['_body']);
				console.log(this.getFretes)
			})
	}

	visualizarDados(message) {
		this.router.navigate([`pages/crud/visualizarFretes/${message._id}`])
	}
	editarDados(message) {
		this.router.navigate([`pages/crud/editarFretes/${message._id}`])

	}
	excluirDados(message) {
		let id = message._id;
		this.http.delete(`${this.api}fretes/${id}`).subscribe(dados => {
			let data = JSON.parse(dados['_body']);
			//this.toastr.success(data.mensagem, 'Concluido!');
			window.location.reload();
		})
	}

	getItems(ev) {
		// Reset items back to all of the items
		this.getFretes;

		// set val to the value of the ev target
		var val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.getFretes = this.getFretes.filter((item) => {
				return (item['nomeCliente'].toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		} else {
			this.ngOnInit();
		}
	}	

	visualizarMotorista(message){
		this.router.navigate([`pages/crud/visualizarMotoristas/${message.id_motorista}`])
	}

}
