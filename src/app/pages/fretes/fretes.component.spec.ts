import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fretes } from './fretes';

describe('FretesComponent', () => {
  let component: Fretes;
  let fixture: ComponentFixture<Fretes>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fretes ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fretes);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
