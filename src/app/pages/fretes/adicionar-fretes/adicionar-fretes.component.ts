import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';


@Component({
    selector: 'adicionar-fretes',
    templateUrl: './adicionar-fretes.html',
})
export class AdicionarFretes {
    dadosFretes: any = {};
    dFretes: any = {};

    constructor(private http: Http,  private router: Router) {
    }


    buscarFretes() {
        this.http.get(`https://servidor-notafacil.herokuapp.com/api/fretes/`, )
            .subscribe(dados => this.populardadosFretes(dados['_body']));
    }

    populardadosFretes(dados) {
        let data = JSON.parse(dados);
        this.dFretes = data[0];
        this.dadosFretes.localColeta = this.dFretes['localColeta'];
        this.dadosFretes.localEntrega = this.dFretes['localEntrega'];
        this.dadosFretes.tipodeMercadoria = this.dFretes['tipodeMercadoria'];
        this.dadosFretes.volume = this.dFretes['volume'];
        this.dadosFretes.quantidade = this.dFretes['quantidade'];
        this.dadosFretes.cubagem = this.dFretes['cubagem'];
        this.dadosFretes.peso = this.dFretes['peso'];
        this.dadosFretes.valorFrete = this.dFretes['valorFrete'];
        this.dadosFretes.status = this.dFretes['status'];
    }

    cadastrar(dados) {
        console.log(dados)
        this.http.post(`https://servidor-notafacil.herokuapp.com/api/fretes/`, dados)
            .subscribe(dados => {
                console.log(dados)
                let data = JSON.parse(dados['_body']);
               // this.toastr.success(data.mensagem, 'Concluido!');
                this.router.navigate([`pages/crud/listarFretes`]);
            })
    }


}
