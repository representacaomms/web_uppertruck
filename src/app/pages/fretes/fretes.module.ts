import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './fretes.routing';

import { Fretes } from './fretes';
import { ListarFretesComponent } from './listar-fretes/listar-fretes.component';
import { EditarFretes } from './editar_fretes';
import { VisualizarFretes } from './visualizar_fretes';
import { AdicionarFretes } from './adicionar-fretes';

import { ThemeModule } from '../../@theme/theme.module';




@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule
  ],
  declarations: [
    Fretes,
    ListarFretesComponent,
    EditarFretes,
    VisualizarFretes,
    AdicionarFretes
  ]
})
export class FretesModule {
}
