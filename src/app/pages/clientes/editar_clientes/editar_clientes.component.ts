import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';


@Component({
    selector: 'editar_clientes',
    templateUrl: './editar_clientes.html',
    styleUrls: ['./editar-clientes.component.scss']

})
export class EditarClientes {
    clientes: any = [];
    id: any;
    dadosClientes: any = {};
    dClientes: any = {};
    listar: any = [];

    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService,
        private router: Router) {

        this.route.params.subscribe(res => {
            this.id = res.id;            
        });

    }

    ngOnInit() {
        this.getListar()

    }
    getListar() {

        let tabela = 'clientes'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })
    }
    populardadosCNPJ(record) {
        console.log(record)
        const dados = record;
        this.dadosClientes.cnpj = dados['cnpj'];
        this.dadosClientes.razao_social = dados['razao_social'];
        this.dadosClientes.fantasia = dados['fantasia'];
        this.dadosClientes.telefone = dados['telefone'];
        this.dadosClientes.email = dados['email'];
        this.dadosClientes.cep = dados['cep'];
        this.dadosClientes.complemento = dados['complemento'];
        this.dadosClientes.endereco = dados['endereco'];
        this.dadosClientes.numero = dados['numero'];
        this.dadosClientes.bairro = dados['bairro'];
        this.dadosClientes.cidade = dados['cidade'];
        this.dadosClientes.estado = dados['estado'];
        this.dadosClientes.segmento = dados['segmento'];
        this.dadosClientes.celular = dados['celular'];
        this.dadosClientes.data_cadastro = dados['data_cadastro'];
        this.dadosClientes.dtNasc = dados['dtNasc'];
        this.dadosClientes.insc_est = dados['insc_est'];
        this.dadosClientes.insc_mun = dados['insc_mun'];
        this.dadosClientes.contato = dados['contato'];
        this.dadosClientes.senha = dados['senha'];

    }
    atualizar(dados) {
        console.log(dados)
        let tabela = 'clientes'
        this.service.updateDoc(tabela, this.id, dados).subscribe(result => {
            this.service.makeToast('success', 'Cliente editando com sucesso!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/clientes/listar-clientes`])
            }, 3500);
        })       

    }

    buscarCep($e) {
        console.log($e)
        this.service.buscarCep($e).subscribe(data => {
            console.log(data)
            this.dadosClientes.razao_social = data['nome'];
            this.dadosClientes.cep = data['cep'];
            this.dadosClientes.complemento = data['complemento'];
            this.dadosClientes.endereco = data['logradouro'];
            this.dadosClientes.numero = data['numero'];
            this.dadosClientes.bairro = data['bairro'];
            this.dadosClientes.cidade = data['localidade'];
            this.dadosClientes.estado = data['uf'];
        });
    }

    
    cancelar() {
        this.service.makeToast('info', 'Informação!', 'Você será redirecionado!');
        setTimeout(() => {
            this.router.navigate([`pages/clientes/listar-clientes`])
        }, 3500);
    }

    
    
    

}