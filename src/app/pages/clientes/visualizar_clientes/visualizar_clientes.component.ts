import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
import { ClientesService } from '../clientes.service';


@Component({
    selector: 'visualizar_clientes',
    templateUrl: './visualizar_clientes.html',
    styleUrls: ['./visualizar-clientes.component.scss']

})
export class VisualizarClientes {
    clientes: any = [];
    id: any;
    dadosClientes: any = {};
    dClientes: any = {};
    listar: any = [];

    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService,
        private router: Router) {

        this.route.params.subscribe(res => {
            this.id = res.id;
        });

    }

    ngOnInit() {      

        this.getListar()


    }
    getListar() {
        let tabela = 'clientes'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardados(result);
        })

    }
    populardados(record) {

        const dados = record;
        this.dadosClientes.cnpj = dados['cnpj'];
        this.dadosClientes.razao_social = dados['razao_social'];
        this.dadosClientes.fantasia = dados['fantasia'];
        this.dadosClientes.telefone = dados['telefone'];
        this.dadosClientes.email = dados['email'];
        this.dadosClientes.cep = dados['cep'];
        this.dadosClientes.complemento = dados['complemento'];
        this.dadosClientes.endereco = dados['endereco'];
        this.dadosClientes.numero = dados['numero'];
        this.dadosClientes.bairro = dados['bairro'];
        this.dadosClientes.cidade = dados['cidade'];
        this.dadosClientes.estado = dados['estado'];
        this.dadosClientes.segmento = dados['segmento'];
        this.dadosClientes.celular = dados['celular'];
        this.dadosClientes.data_cadastro = dados['data_cadastro'];
        this.dadosClientes.dtNasc = dados['dtNasc'];
        this.dadosClientes.insc_est = dados['insc_est'];
        this.dadosClientes.insc_mun = dados['insc_mun'];
        this.dadosClientes.contato = dados['contato'];
        this.dadosClientes.senha = dados['senha'];

    }
    atualizar() {
        this.service.makeToast('success', 'Informação!', 'Você será redirecionado!');
        setTimeout(() => {
            this.router.navigate([`pages/clientes/editar-clientes/${this.id}`])
        }, 3500);
    }
    cancelar() {

        this.service.makeToast('info', 'Informação!', 'Você será redirecionado!');
        setTimeout(() => {
            this.router.navigate([`pages/clientes/listar-clientes`])
        }, 3500);
    }

}