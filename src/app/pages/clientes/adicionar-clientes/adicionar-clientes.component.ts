import { Component, ViewContainerRef, OnInit, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';

import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ClientesService } from '../clientes.service';


@Component({
    selector: 'adicionar-clientes',
    templateUrl: './adicionar-clientes.html',
    styleUrls: ['./adicionar-clientes.component.scss']
})
export class AdicionarClientes implements OnInit {
    dadosClientes: any = {};
    dClientes: any = {};
    value = '';
    eventsReceived = [];

    getClientes: any = [];
    record: any = [];
    firstname: any;
    lastname: any;

    title = 'app works!';
    userStatus = "";
    user = "";
    timeline = [];
    clientes: any;
    cnpj: any;
    firebaseService: any;


    constructor(private http: Http, private router: Router, private service: DataService, public ds: DsService,  @Inject(SESSION_STORAGE) private storage: StorageService, 
    private clientesService: ClientesService ) {
    }

    ngOnInit() {
        this.dadosClientes.data_cadastro = this.service.pegarData();
        console.log(this.dadosClientes.data_cadastro)

    }


    buscarDados($e) {

        let cnpj = $e.replace(/\D/g, '');
        console.log(cnpj)
        if (cnpj < 13) {
            this.service.makeToast('info', 'Informação!', 'CNPJ Invalido, apenas numero!');

        } else {
           
            let obj = { cnpj: cnpj}
            this.service.buscarCNPJ(JSON.stringify(obj)).subscribe(data => {

                if(data['status'] == 'ERROR'){
                    this.service.makeToast('warn', 'Acesso Restrito', data['message']);
                } else {
                    this.populardadosCNPJ(data)
                }
               
              
            })

        }

    }
    populardadosCNPJ(dados) {
        this.dClientes = (dados);
        
        this.dadosClientes.razao_social = this.dClientes['nome'];
        this.dadosClientes.fantasia = this.dClientes['fantasia'];
        this.dadosClientes.telefone = this.dClientes['telefone'];
        this.dadosClientes.email = this.dClientes['email'];
        this.dadosClientes.cep = this.dClientes['cep'];
        this.dadosClientes.complemento = this.dClientes['complemento'];
        this.dadosClientes.endereco = this.dClientes['logradouro'];
        this.dadosClientes.numero = this.dClientes['numero'];
        this.dadosClientes.bairro = this.dClientes['bairro'];
        this.dadosClientes.cidade = this.dClientes['municipio'];
        this.dadosClientes.estado = this.dClientes['uf'];
        this.dadosClientes.segmento = this.dClientes['atividade_principal'][0]['text'];
        this.dadosClientes.dtNasc = this.dClientes['abertura'];
    }
   
    buscarCep($e) {
        console.log($e)
        this.service.buscarCep($e).subscribe(data => {
            console.log(data)
            this.dadosClientes.razao_social = data['nome'];
            this.dadosClientes.cep = data['cep'];
            this.dadosClientes.complemento = data['complemento'];
            this.dadosClientes.endereco = data['logradouro'];
            this.dadosClientes.numero = data['numero'];
            this.dadosClientes.bairro = data['bairro'];
            this.dadosClientes.cidade = data['localidade'];
            this.dadosClientes.estado = data['uf'];
        });
    }

    cadastrar(value){
        let tabela ='clientes';
        this.service.createDoc(value, tabela)
        .subscribe(
          res => {            
            this.service.makeToast('success', 'Concluido!', 'Cadastrado com sucesso!');
            setTimeout(() => {
                this.router.navigate([`pages/clientes/listar-clientes`]); 
            }, 3000);
            
          }
        )
    }

    cancelar() {
        this.service.makeToast('info', 'Concluido!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/clientes/listar-clientes`]); 
            }, 3000);
    }

   
}



