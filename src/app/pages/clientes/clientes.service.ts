import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(public db: AngularFirestore) { }


  createUser(dados) {
    return this.db.collection('clientes').add({
        bairro: dados.bairro,
        celular: dados.celular,
        cep: dados.cep,
        cidade: dados.cidade,
        cnpj:  dados.cnpj,
        complemento: dados.complemento,
        contato: dados.contato,
        //data_cadastro: dados.data_cadastro,
        dtNasc: dados.dtNasc,
        email: dados.email,
        endereco: dados.endereco,
        estado: dados.estado,
        fantasia: dados.fantasia,
        insc_est: dados.insc_est,
        insc_mun: dados.insc_mun,
        numero: dados.numero,
        razao_social: dados.razao_social,
        segmento: dados.segmento,
        telefone: dados.telefone
      /*   nameToSearch: value.name.toLowerCase(), */
        
    });
  }

  getClientes(){
    return new Promise<any>((resolve, reject) => {
       this.db.collection('clientes').snapshotChanges()
      .subscribe(snapshots => {
      resolve(snapshots)
      })
    })
    }

    searchUsers(searchValue){
        return new Observable((observer) => {
          this.db.doc(searchValue).get()
        });
      }
      
      /* return this.db.collection('clientes',ref => ref.where('cnpj', '>=', searchValue)
        .where('cnpj', '<=', searchValue + '\uf8ff'))
        .snapshotChanges() */
    
    
}
