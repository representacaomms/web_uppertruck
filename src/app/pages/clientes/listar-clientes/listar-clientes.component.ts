import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
import { StorageService, SESSION_STORAGE } from 'ngx-webstorage-service';
import { ClientesService } from '../clientes.service';

@Component({
	selector: 'app-listar-clientes',
	templateUrl: './listar-clientes.component.html',
	styleUrls: ['./listar-clientes.component.scss']
})
export class ListarClientesComponent implements OnInit {

	clientes: any = [];
	listar: any = [];
	cnpj: any;

	constructor(private http: Http, private router: Router, public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService,
	private clientesService: ClientesService) {
	}

	ngOnInit() {

		this.getListar();	
	}

	adicionar() {
		this.router.navigate([`pages/clientes/adicionar-clientes`])
	}
	visualizarDados(message) {
		this.router.navigate([`pages/clientes/visualizar-clientes/${message.id}`])
	}
	editarDados(message) {
		this.router.navigate([`pages/clientes/editar-clientes/${message.id}`])

	}
	getItems(ev) {
		// Reset items back to all of the items
		this.clientes;

		// set val to the value of the ev target
		var val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.clientes = this.clientes.filter((item) => {
				return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		} else {
			this.ngOnInit();
		}
	}


	getListar() {
		let tabela =   'clientes';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.listar = resultado			
		}) 
		
	}
	excluirDados(dados) {

	}

}
