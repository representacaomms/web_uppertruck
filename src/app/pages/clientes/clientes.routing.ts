import { Routes, RouterModule }  from '@angular/router';


import { Clientes } from './clientes';
import { ListarClientesComponent } from './listar-clientes/listar-clientes.component';
import { AdicionarClientes } from './adicionar-clientes';
import { VisualizarClientes } from './visualizar_clientes';
import { EditarClientes } from './editar_clientes';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Clientes,
    children: [
      { path: 'listar-clientes', component: ListarClientesComponent},
      { path: 'adicionar-clientes', component: AdicionarClientes},
      { path: 'editar-clientes/:id', component: EditarClientes},
      { path: 'visualizar-clientes/:id', component: VisualizarClientes},

    ]
  }
];

export const routing = RouterModule.forChild(routes);
