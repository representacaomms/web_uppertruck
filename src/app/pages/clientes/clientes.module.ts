import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './clientes.routing';

import { Clientes } from './clientes';
import { AdicionarClientes } from './adicionar-clientes';
import { EditarClientes } from './editar_clientes';
import { VisualizarClientes } from './visualizar_clientes';
import { ListarClientesComponent } from './listar-clientes/listar-clientes.component';

import { ThemeModule } from '../../@theme/theme.module';
import {InputMaskModule} from 'primeng/inputmask';
import {ToastModule} from 'primeng/toast';






@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule,
    InputMaskModule,
    ToastModule
  ],
  declarations: [
    Clientes,
    AdicionarClientes,
    EditarClientes,
    VisualizarClientes,
    ListarClientesComponent    
  ],
})
export class ClientesModule {
}
