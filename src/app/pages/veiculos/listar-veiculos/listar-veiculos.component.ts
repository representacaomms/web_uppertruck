import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';


@Component({
	selector: 'app-listar-veiculos',
	templateUrl: './listar-veiculos.component.html',
	styleUrls: ['./listar-veiculos.component.scss']
})
export class ListarVeiculosComponent  {

	getVeiculos: any = [];
	fotosVeiculo:any;
	DocVeiculo:any;
	veiculos: any = [];
	cnpj: any;
	valOld:any;


	
	constructor(private http: Http, private router: Router,public service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService) {
		this.getListar();

	}

	

	adicionar() {
		this.router.navigate([`pages/veiculos/adicionar-veiculos`])
	}

	visualizarDados(message) {
		this.router.navigate([`pages/veiculos/visualizar-veiculos/${message.id}`])
	}
	editarDados(message) {
		this.router.navigate([`pages/veiculos/editar-veiculos/${message.id}`])

	}
	excluirDados(message) {
	
	}

	getItems(ev) {
		// Reset items back to all of the items
		this.getVeiculos;

		// set val to the value of the ev target
		var val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.getVeiculos = this.getVeiculos.filter((item) => {
				return (item['razao_social'].toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	filtrosItems(ev){

		
		// set val to the value of the ev target
		var val = ev.target.value;

		if(val === ""){
			this.getListar();
		}
			// if the value is an empty string don't filter the items
			if (val && val.trim() != '') {
				this.getVeiculos = this.getVeiculos.filter((item) => {
					return (item['tipo_veiculo'].indexOf(val) > -1);
				})
			}
		
	}

	fotos(message) {
		console.log(message)
		this.router.navigate([`pages/veiculos/fotos/${message.cnpj}`])
	}
	getListar() {
		let tabela =   'veiculos';
		this.service.getAllDoc(tabela).subscribe( result =>{
			
		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;			
			resultado.push(result[key])
		}		
		this.getVeiculos = resultado			
		}) 
		
	}

}
