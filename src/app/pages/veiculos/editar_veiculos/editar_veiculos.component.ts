import { Component, ViewContainerRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';


@Component({
    selector: 'editar_veiculos',
    templateUrl: './editar_veiculos.html',
})
export class EditarVeiculos implements OnInit{
    id: any;
    dadosVeiculos: any = {};
    dVeiculos: any = {};
    veiculos: any = [];
    relProprietarios: any = [];
    relMotoristas: any = [];


    constructor(private http: Http, private route: ActivatedRoute,public service: DataService, public ds: DsService,
        private router: Router) {
        this.route.params.subscribe(res => this.id = res.id);
    }
    ngOnInit() {

        this.getListar()

    }
    getListar() {

        let tabela = 'veiculos'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })
    }

    populardadosCNPJ(dados) {
        this.dVeiculos = dados;
        console.log(this.dVeiculos)
        this.dadosVeiculos.ano  = this.dVeiculos['ano'];
        this.dadosVeiculos.aquisicao  = this.dVeiculos['aquisicao'];
        this.dadosVeiculos.cap_KG  = this.dVeiculos['cap_KG'];
        this.dadosVeiculos.cap_m3  = this.dVeiculos['cap_m3'];
        this.dadosVeiculos.cap_tanque  = this.dVeiculos['cap_tanque'];
        this.dadosVeiculos.celular  = this.dVeiculos['celular'];
        this.dadosVeiculos.celular_motorista  = this.dVeiculos['celular_motorista'];
        this.dadosVeiculos.chassi  = this.dVeiculos['chassi'];
        this.dadosVeiculos.cidade_veiculo  = this.dVeiculos['cidade_veiculo'];
        this.dadosVeiculos.contato  = this.dVeiculos['contato'];
        this.dadosVeiculos.cpf_motorista  = this.dVeiculos['cpf_motorista'];
        this.dadosVeiculos.cnpj  = this.dVeiculos['cnpj'];
        this.dadosVeiculos.descricao  = this.dVeiculos['descricao'];
        this.dadosVeiculos.endereco  = this.dVeiculos['endereco'];
        this.dadosVeiculos.hodometro  = this.dVeiculos['hodometro'];
        this.dadosVeiculos.informacao  = this.dVeiculos['informacao'];
        this.dadosVeiculos.ins_est  = this.dVeiculos['ins_est'];
        this.dadosVeiculos.marca_modelo  = this.dVeiculos['marca_modelo'];
        this.dadosVeiculos.med_carroceria  = this.dVeiculos['med_carroceria'];
        this.dadosVeiculos.media_km  = this.dVeiculos['media_km'];
        this.dadosVeiculos.n_eixos  = this.dVeiculos['n_eixos'];
        this.dadosVeiculos.nome_motorista  = this.dVeiculos['nome_motorista'];
        this.dadosVeiculos.placa  = this.dVeiculos['placa'];
        this.dadosVeiculos.razao_social  = this.dVeiculos['razao_social'];
        this.dadosVeiculos.renavam  = this.dVeiculos['renavam'];
        this.dadosVeiculos.rg_motorista  = this.dVeiculos['rg_motorista'];
        this.dadosVeiculos.tara  = this.dVeiculos['tara'];
        this.dadosVeiculos.telefone  = this.dVeiculos['telefone'];
        this.dadosVeiculos.telefone_motorista  = this.dVeiculos['telefone_motorista'];
        this.dadosVeiculos.tipo_veiculo  = this.dVeiculos['tipo_veiculo'];
        this.dadosVeiculos.uf  = this.dVeiculos['uf'];
    }   

    atualizar(dados) {
        console.log(dados)
        let tabela = 'veiculos'
        this.service.updateDoc(tabela, this.id, dados).subscribe(result => {
            this.service.makeToast('success', 'Veiculo editando com sucesso!', 'Você será redirecionado!');
            setTimeout(() => {
                this.router.navigate([`pages/veiculos/listar-veiculos`])
            }, 3500);
        })       

    }
    cancelar() {
        this.service.makeToast('success', 'Informação', 'Você será redirecionado!');

        setTimeout(() => {
            this.router.navigate([`pages/veiculos/listar-veiculos`])
        }, 3500);
    }

    buscarDadosProprietario($e) {
        for (const key in this.relProprietarios) {
            if (this.relProprietarios[key].cpf == $e) {
                const proprietarios = this.relProprietarios[key]
                console.log(proprietarios)
                 this.populardadosProprietario(proprietarios); 
            } 
        } 
    }

    populardadosProprietario(dados) {
        let data = dados;
        this.dVeiculos = data;
        this.dadosVeiculos.razao_social = this.dVeiculos['nome'];
        this.dadosVeiculos.telefone = this.dVeiculos['fone'];
        this.dadosVeiculos.celular = this.dVeiculos['celular'];
        this.dadosVeiculos.ins_est = this.dVeiculos['rg'];
        this.dadosVeiculos.endereco = this.dVeiculos['endereco'];
        this.dadosVeiculos.ins_est = this.dVeiculos['ins_est'];

    }

    buscarDadosMotorista(e) {
        for (const key in this.relMotoristas) {
           if (this.relMotoristas[key].cpf == e) {
               const motorista = this.relMotoristas[key]
               this.populardadosMotorista(motorista);
           } 
       } 

   }

   populardadosMotorista(dados) {
       let data = dados;
       this.dVeiculos = data;
       this.dadosVeiculos.nome_motorista = this.dVeiculos['nome'];
       this.dadosVeiculos.cpf_motorista = this.dVeiculos['cpf'];
       this.dadosVeiculos.rg_motorista = this.dVeiculos['rg'];
       this.dadosVeiculos.telefone_motorista = this.dVeiculos['fone1'];
       this.dadosVeiculos.celular_motorista = this.dVeiculos['celular'];
   }


}
