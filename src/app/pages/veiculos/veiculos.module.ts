import { HttpModule } from '@angular/http';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './veiculos.routing';

import { Veiculos } from './veiculos';
import { ListarVeiculosComponent } from './listar-veiculos/listar-veiculos.component';
import { EditarVeiculos } from './editar_veiculos';
import { VisualizarVeiculos } from './visualizar_veiculos';
import { AdicionarVeiculos } from './adicionar-veiculos';
import { Fotos } from './fotos';
import { ThemeModule } from '../../@theme/theme.module';

import {DropdownModule} from 'primeng/dropdown';
import {InputMaskModule} from 'primeng/inputmask';
import {ToastModule} from 'primeng/toast';





@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgbRatingModule,
    routing,
    HttpModule,
    ThemeModule,
    DropdownModule,
    InputMaskModule,
    ToastModule
  ],
  declarations: [
    Veiculos,
    AdicionarVeiculos,
    ListarVeiculosComponent,
    EditarVeiculos,
    VisualizarVeiculos,
    Fotos
  ]
})
export class VeiculosModule {
}
