import { Routes, RouterModule }  from '@angular/router';


import { Veiculos } from './veiculos';
import { ListarVeiculosComponent } from './listar-veiculos/listar-veiculos.component';
import { EditarVeiculos } from './editar_veiculos';
import { VisualizarVeiculos } from './visualizar_veiculos';
import { AdicionarVeiculos } from './adicionar-veiculos';
import { Fotos } from './fotos';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Veiculos,
    children: [
      { path: 'listar-veiculos', component: ListarVeiculosComponent}, 
      { path: 'editar-veiculos/:id', component: EditarVeiculos}, 
      { path: 'visualizar-veiculos/:id', component: VisualizarVeiculos}, 
      { path: 'adicionar-veiculos', component: AdicionarVeiculos}, 
      { path: 'fotos/:id', component: Fotos}, 

    ]
  }
];

export const routing = RouterModule.forChild(routes);
