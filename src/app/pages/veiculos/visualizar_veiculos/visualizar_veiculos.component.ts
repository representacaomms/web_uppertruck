import { Component, ViewContainerRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';


@Component({
    selector: 'visualizar_veiculos',
    templateUrl: './visualizar_veiculos.html',
})
export class VisualizarVeiculos implements OnInit {
    id: any;
    dadosVeiculos: any = {};
    dVeiculos: any = {};
    veiculos: any = [];
    constructor(private http: Http, private route: ActivatedRoute, public service: DataService, public ds: DsService,
        private router: Router) {
        this.route.params.subscribe(res => this.id = res.id);
    }

    ngOnInit() {

       

        this.getListar()      


    }

    getListar() {

        let tabela = 'veiculos'
        this.service.getDoc(tabela, this.id).subscribe(result => {
            this.populardadosCNPJ(result);
        })
    }
   

    populardadosCNPJ(dados) {
        this.dVeiculos = dados;
        console.log(this.dVeiculos)
        this.dadosVeiculos.ano  = this.dVeiculos['ano'];
        this.dadosVeiculos.aquisicao  = this.dVeiculos['aquisicao'];
        this.dadosVeiculos.cap_KG  = this.dVeiculos['cap_KG'];
        this.dadosVeiculos.cap_m3  = this.dVeiculos['cap_m3'];
        this.dadosVeiculos.cap_tanque  = this.dVeiculos['cap_tanque'];
        this.dadosVeiculos.celular  = this.dVeiculos['celular'];
        this.dadosVeiculos.celular_motorista  = this.dVeiculos['celular_motorista'];
        this.dadosVeiculos.chassi  = this.dVeiculos['chassi'];
        this.dadosVeiculos.cidade_veiculo  = this.dVeiculos['cidade_veiculo'];
        this.dadosVeiculos.contato  = this.dVeiculos['contato'];
        this.dadosVeiculos.cpf_motorista  = this.dVeiculos['cpf_motorista'];
        this.dadosVeiculos.cnpj  = this.dVeiculos['cnpj'];
        this.dadosVeiculos.descricao  = this.dVeiculos['descricao'];
        this.dadosVeiculos.endereco  = this.dVeiculos['endereco'];
        this.dadosVeiculos.hodometro  = this.dVeiculos['hodometro'];
        this.dadosVeiculos.informacao  = this.dVeiculos['informacao'];
        this.dadosVeiculos.ins_est  = this.dVeiculos['ins_est'];
        this.dadosVeiculos.marca_modelo  = this.dVeiculos['marca_modelo'];
        this.dadosVeiculos.med_carroceria  = this.dVeiculos['med_carroceria'];
        this.dadosVeiculos.media_km  = this.dVeiculos['media_km'];
        this.dadosVeiculos.n_eixos  = this.dVeiculos['n_eixos'];
        this.dadosVeiculos.nome_motorista  = this.dVeiculos['nome_motorista'];
        this.dadosVeiculos.placa  = this.dVeiculos['placa'];
        this.dadosVeiculos.razao_social  = this.dVeiculos['razao_social'];
        this.dadosVeiculos.renavam  = this.dVeiculos['renavam'];
        this.dadosVeiculos.rg_motorista  = this.dVeiculos['rg_motorista'];
        this.dadosVeiculos.tara  = this.dVeiculos['tara'];
        this.dadosVeiculos.telefone  = this.dVeiculos['telefone'];
        this.dadosVeiculos.telefone_motorista  = this.dVeiculos['telefone_motorista'];
        this.dadosVeiculos.tipo_veiculo  = this.dVeiculos['tipo_veiculo'];
        this.dadosVeiculos.uf  = this.dVeiculos['uf'];
    }   

    editar() {
        this.service.makeToast('sucess','Informação!','Você será redirecionado');
        setTimeout(() => {
            this.router.navigate([`pages/veiculos/editar-veiculos/${this.id}`])
        }, 3500) ;
    }

    cancelar() {
        this.service.makeToast('sucess','Informação!','Você será redirecionado');
        setTimeout(() => {
            this.router.navigate([`pages/veiculos/listar-veiculos`])
        }, 3500);
    }


}
