import { Component, ViewContainerRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'fotos',
    templateUrl: './fotos.html',
})
export class Fotos {
    imagePathFoto:any;
    imagePathDoc:any;

    constructor(private http: Http, private route: ActivatedRoute, 
        private router: Router) {
            this.route.params.subscribe(res => {
                this.buscarFotos(res.id)
            });
    }
    buscarFotos(cpf) {
        this. imagePathDoc= 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/fotos%2F' + cpf +'%2Fdoc-veiculo?alt=media';
		this. imagePathFoto= 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/fotos%2F' + cpf +'%2Ffoto-veiculo?alt=media';
    }

}
