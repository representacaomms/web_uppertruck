import { Component, ViewContainerRef, Inject, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { SelectItem } from 'primeng/api';




@Component({
  selector: 'adicionar-veiculos',
  templateUrl: './adicionar-veiculos.html',
})
export class AdicionarVeiculos implements OnInit {
  dadosVeiculos: any = {};
  dVeiculos: any = {};
  veiculos: any;
  cnpj: any;
  proprietarios: any = [];
  motoristas: any = [];
  relProprietarios: any = [];
  relMotoristas: any = [];
  relVeiculos: any = [];
  tipo_veiculo: SelectItem[];
  n_eixos: SelectItem[];


  constructor(private http: Http, private router: Router, private service: DataService, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService, ) {
  }
  ngOnInit() {

    this.dadosVeiculos.data_cadastro = this.service.pegarData();
    this.getProprietarios()
    this.getMotoristas()

    this.n_eixos = [
      { value: '2', label: '2' },
      { value: '3', label: '3' },
      { value: '4', label: '4' },
      { value: '5', label: '5' },
      { value: '6', label: '6' },
      { value: '7', label: '7' },
      { value: '8', label: '8' },
      { value: '9', label: '9' },

    ];
    this.tipo_veiculo = [
      { value: 'Motocicleta', label: 'Motocicleta' },
      { value: 'Utilitário', label: 'Utilitário' },
      { value: 'VUC', label: 'VUC' },
      { value: 'Toco Baú', label: 'Toco Baú' },
      { value: 'Toco Aberto', label: 'Toco Aberto' },
      { value: 'Truck Aberto', label: 'Truck Aberto' },
      { value: 'Truck Fechado', label: 'Truck Fechado' },
      { value: 'Truck Sider', label: 'Truck Sider' },
      { value: 'SR Aberto', label: 'SR Aberto' },
      { value: 'SR Fechado', label: 'SR Fechado' },
      { value: 'Cavalo 4X2', label: 'Cavalo 4X2' },
      { value: 'Cavalo 6X2', label: 'Cavalo 6X2' },
      { value: 'Cavalo 6X2 T', label: 'Cavalo 6X2 T' },
      { value: 'Cavalo 6X4', label: 'Cavalo 6X4' },
      { value: 'Cavalo 8X4', label: 'Cavalo 8X4' },

    ];
  }
  getProprietarios() {
    let tabela = 'proprietarios';
    this.service.getAllDoc(tabela).subscribe(result => {

      const resultado = [];
      for (const key in result) {
        result[key].id = key;
        resultado.push(result[key])
      }
      this.relProprietarios = resultado
    })
  }
  getMotoristas() {
    let tabela = 'motoristas';
    this.service.getAllDoc(tabela).subscribe(result => {
      const resultado = [];
      for (const key in result) {
        result[key].id = key;
        resultado.push(result[key])
      }
      this.relMotoristas = resultado
    })
  }


  buscarDadosProprietario($e) {
    var proprietario = false;
    this.resetarProprietario();
    for (const key in this.relProprietarios) {
      if (this.relProprietarios[key].cpf == $e) {
        const proprietarios = this.relProprietarios[key]
        console.log(proprietarios)
        proprietario = true;
        this.populardadosProprietario(proprietarios);
      }
    }
    if (proprietario === false) {
      this.service.makeToast('danger', 'Alerta!', 'Proprietário não encontrado!');
    }

  }
  populardadosProprietario(dados) {
    console.log('POPULAR DADOS PROPRIETARIO', dados)

    let data = dados;
    this.dVeiculos = data;
    this.dadosVeiculos.razao_social = this.dVeiculos['nome'];
    this.dadosVeiculos.telefone = this.dVeiculos['fone'];
    this.dadosVeiculos.celular = this.dVeiculos['celular'];
    this.dadosVeiculos.ins_est = this.dVeiculos['rg'];
    this.dadosVeiculos.endereco = this.dVeiculos['endereco'];
    this.dadosVeiculos.ins_est = this.dVeiculos['ins_est'];

  }

  buscarDadosMotorista(e) {

    var motorista = false
    this.resetarMotorista();
    for (const key in this.relMotoristas) {
      if (this.relMotoristas[key].cpf == e) {
        motorista = true;
        const motoristas = this.relMotoristas[key];
        this.populardadosMotorista(motoristas);
      }
    }
    if (motorista === false) {
      this.service.makeToast('error', 'Alerta!', 'Motorista não encontrado!');
    }

  }

  populardadosMotorista(dados) {

    console.log('POPULAR DADOS MOTORISTA', dados)

    let data = dados;
    this.dVeiculos = data;
    this.dadosVeiculos.nome_motorista = this.dVeiculos['nome'];
    this.dadosVeiculos.cpf_motorista = this.dVeiculos['cpf'];
    this.dadosVeiculos.rg_motorista = this.dVeiculos['rg'];
    this.dadosVeiculos.telefone_motorista = this.dVeiculos['fone1'];
    this.dadosVeiculos.celular_motorista = this.dVeiculos['celular'];
    this.dadosVeiculos.placa = this.dVeiculos['placa'];
  }

  cadastrar(value) {
    let tabela = 'veiculos';
    this.service.createDoc(value, tabela)
      .subscribe(
        res => {
          this.service.makeToast('success', 'Concluido!', 'Cadastrado com sucesso!');
          setTimeout(() => {
            this.router.navigate([`pages/veiculos/listar-veiculos`]);
          }, 3000);

        }
      )
  }

  resetarMotorista() {
    this.dadosVeiculos.nome_motorista = '';
    this.dadosVeiculos.cpf_motorista = '';
    this.dadosVeiculos.rg_motorista = '';
    this.dadosVeiculos.telefone_motorista = '';
    this.dadosVeiculos.celular_motorista = '';
  }
  resetarProprietario() {
    this.dadosVeiculos.razao_social = '';
    this.dadosVeiculos.telefone = '';
    this.dadosVeiculos.celular = '';
    this.dadosVeiculos.ins_est = '';
    this.dadosVeiculos.endereco = '';
    this.dadosVeiculos.ins_est = '';

  }


}
