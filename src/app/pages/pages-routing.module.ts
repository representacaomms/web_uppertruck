import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { CanhotosComponent } from './canhotos/canhotos.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { AuthGuard } from '../guards/auth-guard';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: ECommerceComponent,
  }, {
    path: 'iot-dashboard',
    component: DashboardComponent,
  },{
    path: 'canhotos',
    component: CanhotosComponent,
  },
  {
    path: 'relatorios',
    component: RelatoriosComponent,
  },
  {
    path: 'clientes',
    loadChildren: './clientes/clientes.module#ClientesModule',
  },  {
    path: 'proprietarios',
    loadChildren: './proprietarios/proprietarios.module#ProprietariosModule',
  },  {
    path: 'motoristas',
    loadChildren: './motoristas/motoristas.module#MotoristasModule',
  },{
    path: 'veiculos',
    loadChildren: './veiculos/veiculos.module#VeiculosModule',
  },{
    path: 'fretes',
    loadChildren: './fretes/fretes.module#FretesModule',
  },{
    path: 'romaneio',
    loadChildren: './romaneio/romaneio.module#RomaneioModule',
  },{
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  },{
    path: 'representantes',
    loadChildren: './representantes/representantes.module#RepresentantesModule',
  }, {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: '', redirectTo: 'iot-dashboard', pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
