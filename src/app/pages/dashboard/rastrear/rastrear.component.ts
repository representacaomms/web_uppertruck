import { Component, ViewChild, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { DataService } from '../../../data.service';
import { DsService } from '../../../ds.service';
import { AngularFireDatabase } from '@angular/fire/database';


declare var google: any;


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'rastrear',
    templateUrl: './rastrear.html',
})
export class RastrearComponent implements OnInit {

    latitude: number;
    longitude: number;
    isTracking: any;
    currentLat: any;
    currentLong: any;
    marker: any;
    key = 'AIzaSyAVwITJ9kcs9RbeuKgpTZI1Qd39q51Nv04';
    pesquisa: any;
    posicao: any = [];
    posicaoEntrega: any;
    lng: any;
    lat: any;
    entregas: any = [];
    coletas: any = [];
    @ViewChild('gmap') gmapElement: any;
    map: any; // google.maps.Map;
    id_motorista = '5afb4da872d5b20004284ec2';
    romaneio_lat: any;
    romaneio_lng: any;
    posicaoCaminhao: any = [];
    romaneio: any = {};
    client: any;
    driver: any = [];
    latPosition: any;
    lngPosition: any;
    listDriver: any = {};
    updateMapPointer: any;
    coordenadas: any;
    data: any;
    markers = [];
    constructor(private http: Http, public ds: DsService, public service: DataService, public db: AngularFireDatabase) {
    }
    ngOnInit() {

        this.openMapa()
    }


    openMapa() {
        var image = {
// tslint:disable-next-line: max-line-length
            url: 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/mascote.png?alt=media&token=0674dab3-7b0b-4075-88ac-9a6718afcd31',
        }
        var mudancaPos:any = [];
        const iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var markers = [];

        navigator.geolocation.getCurrentPosition((coords) => {
            this.posicao = new google.maps.LatLng(coords['coords'].latitude, coords['coords'].longitude);

            const mapOptions = {
                center: this.posicao,
                zoom: 10
            };

            const map = new google.maps.Map(this.gmapElement.nativeElement, mapOptions);
            var dbRef = this.db.database.ref('geolocations');
            dbRef.on('child_added' , function ( data ) {
                var uluru = { lat: data.val().latitude, lng: data.val().longitude };
                var marker = new google.maps.Marker({
                    position: uluru,
                    // Animção
                    animation: google.maps.Animation.ROUND, // BOUNCE
                    // Icone
                    icon: image,
                    map: map,
                    title: data.val().nome,
                });
                markers[data.key] = marker; // add marker in the markers array...
            });
            dbRef.on('child_changed', function (data) {
                markers[data.key].setMap(null);
                var uluru = { lat: data.val().latitude, lng: data.val().longitude };
                var marker = new google.maps.Marker({
                    position: uluru,
                    // Animção
                    animation: google.maps.Animation.ROUND, // BOUNCE
                    // Icone
                    icon: image,
                    map: map,
                    title: data.val().nome,
                });
                markers[data.key] = marker; // add marker in the markers array...
            });

        });
    }

}
