import { Component, Input } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    selector: 'ngx-status-card',
    styleUrls: ['./status-card.component.scss'],
    templateUrl: './status-card.component.html',

})
export class StatusCardComponent {

    @Input() title: string;
    @Input() type: string;

    constructor(private http: Http, private router: Router) {
    }

    abrirPagina(item) {

        switch (item) {
            case 'Clientes':
                this.router.navigate([`pages/clientes/listar-clientes`]);
                break;
            case 'Proprietários':
                this.router.navigate([`pages/proprietarios/listar-proprietarios`]);
                break;
            case 'Motoristas':
                this.router.navigate([`pages/motoristas/listar-motoristas`]);
                break;
            case 'Veiculos':
                this.router.navigate([`pages/veiculos/listar-veiculos`]);
                break;
            case 'Romaneios':
                this.router.navigate([`pages/romaneio/listar-romaneios`]);
                break;
            case 'Canhotos':
                this.router.navigate([`pages/canhotos`]);
                break;
            case 'Relatorios':
                this.router.navigate([`pages/relatorios`]);
                break;

        }


    }
}

