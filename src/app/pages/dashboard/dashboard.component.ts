import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {

  private alive = true;

  clientesCard: CardSettings = {
    title: 'Clientes',
    iconClass: 'nb-person',
    type: 'primary',
  };
  credenciadoCard: CardSettings = {
    title: 'Relatorios',
    iconClass: 'nb-person',
    type: 'primary',
  };
 
  fretesCard: CardSettings = {
    title: 'Faturamento (Em Desenvolvimento)',
    iconClass: 'nb-person',
    type: 'primary',
  };
  proprietarioCard: CardSettings = {
    title: 'Proprietários',
    iconClass: 'nb-person',
    type: 'primary',
  };
  romaneioCard: CardSettings = {
    title: 'Romaneios',
    iconClass: 'nb-snowy-circled',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Motoristas',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Veiculos',
    iconClass: 'nb-snowy-circled',
    type: 'warning',
  };

  canhotosCard: CardSettings = {
    title: 'Canhotos',
    iconClass: 'nb-person',
    type: 'primary',
  };


  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.clientesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
    this.proprietarioCard,
    this.fretesCard,
    this.credenciadoCard,
    this.romaneioCard,
    this.canhotosCard
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.clientesCard,
        type: 'warning',
      },
     
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
     
      {
        ...this.proprietarioCard,
        type: 'warning',
      },
      {
        ...this.fretesCard,
        type: 'primary',
      },
      {
        ...this.credenciadoCard,
        type: 'primary',
      }, 
      {
        ...this.romaneioCard,
        type: 'primary',
      },
      {
        ...this.canhotosCard,
        type: 'primary',
      },
    ],
  };

  constructor(private themeService: NbThemeService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });

   
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
