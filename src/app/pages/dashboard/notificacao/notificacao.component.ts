import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../../../data.service';

@Component({
  selector: 'ngx-notificacao',
  templateUrl: './notificacao.component.html',
  styleUrls: ['./notificacao.component.scss'],
  providers: [MessageService]
})
export class NotificacaoComponent implements OnInit {

  dadosMensagem: any = {};
  contatos_Enviar: any = [];


  constructor(public http: HttpClient, private dataSerivce: DataService, private messageService: MessageService) { }

  ngOnInit() {
    this.dadosMensagem.id_aplicativo = 'aa2ed3c3-a847-4b48-883c-0d246d846075';
    this.dadosMensagem.included_segments = ["All"];
  }

  enviar(dados) {
    let value = {
      app_id: dados.id_aplicativo,
      included_segments: dados.included_segments,
      contents: { "en": dados.contents },
      headings: { "en": dados.headings }
    }

    let json = JSON.stringify(value)
    const Hheaders: HttpHeaders = new HttpHeaders()
      .set('authorization', 'Basic  MjMzOTE3YmQtNTBlMC00NDg2LWE1NTItNjE3ZDgyZDI1YjJm')
    this.http.post('https://onesignal.com/api/v1/notifications', value, { headers: Hheaders }).subscribe(data => {

      if (data['errors']) {
        console.log(data)
        this.addSingle('error', 'Erro', data['errors']);
      } else {
        this.addSingle('success', 'Parabéns', 'Mensagem enviada com sucesso!');
        this.dadosMensagem.id_aplicativo = 'aa2ed3c3-a847-4b48-883c-0d246d846075';
        this.dadosMensagem.included_segments = ["All"];
        this.dadosMensagem.headings = '';
        this.dadosMensagem.contents = '';
      }
    }, error => {
      console.error(error)
      this.addSingle('error', 'Erro', error.error.errors[0]);
    })

  }

  addSingle(status, titulo, conteudo) {
    this.messageService.add({ severity: status, summary: titulo, detail: conteudo });
  }
  cancelar() {
    this.dadosMensagem.id_aplicativo = 'aa2ed3c3-a847-4b48-883c-0d246d846075';
    this.dadosMensagem.included_segments = ["All"];
    this.dadosMensagem.headings = '';
    this.dadosMensagem.contents = '';
  }

}