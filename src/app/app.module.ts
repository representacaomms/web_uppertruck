/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DsService } from './ds.service';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { LoginComponent } from './pages/login/login.component';
import { DataService } from './data.service';
import { ToasterService } from 'angular2-toaster';
import { ToasterModule } from 'angular2-toaster';

import {InputMaskModule} from 'primeng/inputmask';
import { environment } from '../environments/environment';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';


import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';

import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { ModalsComponent } from './@theme/components/theme-settings/modals/modals.component';
import { ModalComponent } from './@theme/components/theme-settings/modals/modal/modal.component';
import { NbBadgeModule } from '@nebular/theme';
import { RastreamentoClienteComponent } from './pages/rastreamento-cliente/rastreamento-cliente.component';
import { AuthGuard } from './guards/auth-guard';


@NgModule({
  entryComponents: [
    ModalComponent,

  ],
  declarations: [AppComponent, LoginComponent, ModalsComponent,
    ModalComponent, RastreamentoClienteComponent],


  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    StorageServiceModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    ToasterModule.forRoot(),
    InputMaskModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    ToastModule,
    ConfirmDialogModule,
    NbBadgeModule,


  ],
  bootstrap: [AppComponent],
  providers: [DsService,DataService,ToasterService, AngularFirestore,
    { provide: APP_BASE_HREF, useValue: '/' },MessageService,
    ModalsComponent , AuthGuard
  ],
})
export class AppModule {
}
