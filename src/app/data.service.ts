import { Injectable, Inject } from '@angular/core';
import { } from '@angular/http';
import { DsService } from './ds.service';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import 'rxjs/add/operator/map';
import 'style-loader!angular2-toaster/toaster.css';
import { HttpClient } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable()
export class DataService {
  webService = 'http://35.196.168.29/WebService-Uppertruck/';
  webServiceFirebase = 'https://my-project-1520872899122.firebaseio.com/';
  cnpj: any;


  constructor(private http: HttpClient, public ds: DsService, @Inject(SESSION_STORAGE) private storage: StorageService, private toasterService: ToasterService,
    private messageService: MessageService, public db: AngularFireDatabase) {
    this.cnpj = this.storage.get('cnpj');
    this.pegarData();
  }
  buscarCep(cep) {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`);
  }

  usuaurioAuth: boolean = false;
  config: ToasterConfig;

  position = 'toast-top-right';
  animationType = 'fade';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  timeout = 5000;
  toastsLimit = 5;
  type = 'default';

  isNewestOnTop = true;
  isHideOnClick = true;
  isDuplicatesPrevented = false;
  isCloseButton = true;

  types: string[] = ['default', 'info', 'success', 'warning', 'error'];
  animations: string[] = ['fade', 'flyLeft', 'flyRight', 'slideDown', 'slideUp'];
  positions: string[] = ['toast-top-full-width', 'toast-bottom-full-width', 'toast-top-left', 'toast-top-center',
    'toast-top-right', 'toast-bottom-right', 'toast-bottom-center', 'toast-bottom-left', 'toast-center'];

  quotes = [
    { title: null, body: 'We rock at <i>Angular</i>' },
    { title: null, body: 'Titles are not always needed' },
    { title: null, body: 'Toastr rock!' },
    { title: 'What about nice html?', body: '<b>Sure you <em>can!</em></b>' },
  ];

  loginToast(status, titulo, mensagem) {
    this.showToast(status, titulo, mensagem);
  }

  makeToast(status, titulo, mensagem) {
    this.messageService.add({ severity: status, summary: titulo, detail: mensagem });
  }

  openRandomToast() {
    const typeIndex = Math.floor(Math.random() * this.types.length);
    const quoteIndex = Math.floor(Math.random() * this.quotes.length);
    const type = this.types[typeIndex];
    const quote = this.quotes[quoteIndex];

    this.showToast(type, quote.title, quote.body);
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: this.position,
      timeout: this.timeout,
      newestOnTop: this.isNewestOnTop,
      tapToDismiss: this.isHideOnClick,
      preventDuplicates: this.isDuplicatesPrevented,
      animation: this.animationType,
      limit: this.toastsLimit,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: this.timeout,
      showCloseButton: this.isCloseButton,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  clearToasts() {
    this.toasterService.clear();
  }
  pegarData() {
    const date = new Date();
    const dia = date.getDate();
    var mes = date.getMonth() + 1;
    if (mes < 10) {
      const mes_atual = '0' + mes;
      const ano = date.getFullYear();
      const data = ano + '-' + mes_atual + '-' + dia;
      return data;
    } else {
      const ano = date.getFullYear();
      const data = ano + '-' + mes + '-' + dia;
      return data;
    }


  }



  fazerLogin(data) {
    return this.http.post(this.webService + 'logar.php', data).map(res => res);
  }

  logout(data) {
    return this.http.post(this.webService + 'logout.php', data).map(res => res);

  }

  buscarCNPJ(data) {
    return this.http.post(this.webService + 'buscarCNPJ.php', data).map(res => res);
  }

  createDoc(data, tabela) {
    return this.http.post(this.webServiceFirebase + tabela + '.json', data).map(res => res);
  }
  getAllDoc(tabela) {
    return this.http.get(this.webServiceFirebase + tabela + '.json').map(res => res);
  }
  getDoc(tabela, id) {
    return this.http.get(this.webServiceFirebase + tabela + '/' + id + '.json').map(res => res);
  }
  updateDoc(tabela, id, dados) {
    return this.http.put(this.webServiceFirebase + tabela + '/' + id + '.json', dados).map(res => res);
  }

  delDoc(tabela, id) {
    return this.http.delete(this.webServiceFirebase + tabela + '/' + id + '.json').map(res => res);
  }

  getAll() {
    return this.db.list('geolocations').snapshotChanges();
  }

  getCoordsAll() {
    /*  var commentsRef = this.firebase.database().ref('coords/' + postId);
     commentsRef.on('child_changed', function(data) {

       }); */
  }

  getAllFotos() {
    const array = [
      'https://firebasestorage.googleapis.com/v0/b/teste-uppertruck.appspot.com/o/canhotos%2Fcanhoto-1.png?alt=media',
      'https://firebasestorage.googleapis.com/v0/b/teste-uppertruck.appspot.com/o/canhotos%2Fcanhoto-2.jpg?alt=media',
      'https://firebasestorage.googleapis.com/v0/b/teste-uppertruck.appspot.com/o/canhotos%2Fcanhoto-3.jpg?alt=media',
    ];

    return array;
    /*     // Crie uma referência com um caminho de arquivo inicial e nome
    var storage = this.storage();
    var pathReference = storage.ref('images/stars.jpg');

    // Crie uma referência de um URI do Google Cloud Storage
    var gsReference = storage.refFromURL('gs://bucket/images/stars.jpg')

    // Cria uma referência a partir de um URL HTTPS
    // Observe que no URL, os caracteres são escapados do URL!
    var httpsReference = storage.refFromURL('https://firebasestorage.googleapis.com/b/bucket/o/images%20stars.jpg'); */
  }

  loginAuth(dados: boolean) {

    this.usuaurioAuth = dados;
  }

  usuarioEstaAuth() {
    return this.usuaurioAuth;
  }
}
