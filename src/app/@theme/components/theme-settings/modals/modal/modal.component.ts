import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-modal',
  templateUrl: './modal.component.html',
})
export class ModalComponent implements OnInit {

  modalHeader: string;
  listar: any = [];
  conteudo = this.conteudo;
  imagePathFoto: any;
  imagePathDoc: any;

  constructor(private activeModal: NgbActiveModal, private service: DataService, private router: Router, ) { }

  ngOnInit() {
    let tabela = 'cad_Aplicativo';
    this.service.getAllDoc(tabela).subscribe(result => {

      const resultado = [];
// tslint:disable-next-line: forin
      for (const key in result) {
        result[key].id = key;
        // tslint:disable-next-line: max-line-length
        result[key].imagePathFoto = 'https://firebasestorage.googleapis.com/v0/b/my-project-1520872899122.appspot.com/o/fotos%2F' + result[key].cnpj + '%2Fcnh?alt=media',

          resultado.push(result[key])
      }
      this.listar = resultado
    })

  }


  delete(value) {
    let tabela = 'cad_Aplicativo';
    this.service.delDoc(tabela, value.id).subscribe(data => {
      this.router.navigate([`pages/motoristas/mobile-motoristas/${value.cpf}`]);
    })

  }


  visualizar(value){
    this.router.navigate([`pages/motoristas/mobile-motoristas/${value.cpf}`]);
  }
}
