import { Component, Input, OnInit, Inject } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { LayoutService } from '../../../@core/data/layout.service';
import { StorageService, SESSION_STORAGE } from 'ngx-webstorage-service';
import { DsService } from '../../../ds.service';
import { Router } from '@angular/router';

import { ModalsComponent } from '../theme-settings/modals/modals.component';
import { DataService } from '../../../data.service';

@Component({
    selector: 'ngx-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

    @Input() position = 'normal';

    user: any;
    tb_logados: any;

    userMenu = [{ title: 'Profile' }, { title: 'Log out', funcao: 'sair' }];
    listar: any = [];


    constructor(private sidebarService: NbSidebarService,
        private menuService: NbMenuService,
        private userService: UserService,
        private analyticsService: AnalyticsService,
        private layoutService: LayoutService,
        @Inject(SESSION_STORAGE) private storage: StorageService,
        public ds: DsService,
        private modal: ModalsComponent,
        private service: DataService,
        private router: Router) {
    }

    ngOnInit() {
        this.userService.getUsers()
            .subscribe((users: any) => this.user = users.nick);

            setInterval(() => { this.getCadastros(); },30000);
    }

    getCadastros() {
        let tabela =   'cad_Aplicativo';
		this.service.getAllDoc(tabela).subscribe( result =>{

		 const resultado =  [];
		for (const key in result) {
			result[key].id = key;
			resultado.push(result[key])
		}
        this.listar = resultado.length;
        if(this.listar !== 0){
            this.service.makeToast('success', 'Cadastros em Aberto!', `Exitem ${this.listar} - Cadastros!`);
        }
		})

    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(true, 'menu-sidebar');
        this.layoutService.changeLayoutSize();

        return false;
    }

    toggleSettings() {
       this.modal.showLargeModal();
    }

    goToHome() {
        this.menuService.navigateHome();
    }

    startSearch() {
        this.analyticsService.trackEvent('startSearch');
    }
    logout() {

        this.storage.remove('idLogin')
        this.storage.remove('cnpj');
        this.storage.remove('empresa');
        this.storage.remove('status');
        this.router.navigate(['auth/login']);
        location.reload();
    }
}
