/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDbH22zdcPx9rv0IOMiiGq9lw0T2IhVXKg",
    authDomain: "my-project-1520872899122.firebaseapp.com",
    databaseURL: "https://my-project-1520872899122.firebaseio.com",
    projectId: "my-project-1520872899122",
    storageBucket: "my-project-1520872899122.appspot.com",
    messagingSenderId: "785749493075"
  }
};
